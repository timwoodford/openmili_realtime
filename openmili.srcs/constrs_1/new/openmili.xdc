#Setting the Configuration Bank Voltage Select
set_property CFGBVS GND [current_design]
set_property CONFIG_VOLTAGE 1.8 [current_design]

create_clock -period 10.000 -name sys_clk [get_ports PCIE_REFCLK_P]
#reset
set_property -dict {PACKAGE_PIN AN8 IOSTANDARD LVCMOS18} [get_ports sys_rst]

#uart
set_property -dict {PACKAGE_PIN K26 IOSTANDARD LVCMOS18} [get_ports uart_sout]
set_property -dict {PACKAGE_PIN G25 IOSTANDARD LVCMOS18} [get_ports uart_sin]

#Fan
set_property -dict {PACKAGE_PIN AJ9 IOSTANDARD LVCMOS18} [get_ports fan_pwm]

set_clock_groups -name async1 -asynchronous -group [get_clocks -include_generated_clocks -of_objects [get_ports PCIE_REFCLK_P]] -group [get_clocks -include_generated_clocks -of_objects [get_pins [all_fanin -flat -startpoints_only [get_pins -hier -filter name=~*/gt_top_i/phy_clk_i/CLK_USERCLK_IN]]]]


set_false_path -from [get_ports PCIE_PERST_B_LS]
set_property LOC PCIE_3_1_X0Y0 [get_cells xillybus_ins/pcie/inst/pcie3_uscale_top_inst/pcie3_uscale_wrapper_inst/PCIE_3_1_inst]
set_property PACKAGE_PIN K22 [get_ports PCIE_PERST_B_LS]
set_property LOC GTHE3_COMMON_X0Y1 [get_cells -hier -filter name=~*/pcieclk_ibuf]
set_property IOSTANDARD LVCMOS18 [get_ports PCIE_PERST_B_LS]
set_property PULLUP true [get_ports PCIE_PERST_B_LS]

# sw/led

set_property -dict "PACKAGE_PIN AP8 IOSTANDARD LVCMOS18" [get_ports "GPIO_LED[0]"]
set_property -dict "PACKAGE_PIN H23 IOSTANDARD LVCMOS18" [get_ports "GPIO_LED[1]"]
set_property -dict "PACKAGE_PIN P20 IOSTANDARD LVCMOS18" [get_ports "GPIO_LED[2]"]
set_property -dict "PACKAGE_PIN P21 IOSTANDARD LVCMOS18" [get_ports "GPIO_LED[3]"]

#set_property -dict {PACKAGE_PIN AP8 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[0]}]
#set_property -dict {PACKAGE_PIN H23 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[1]}]
#set_property -dict {PACKAGE_PIN P20 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[2]}]
#set_property -dict {PACKAGE_PIN P21 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[3]}]

set_property -dict {PACKAGE_PIN N22 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[4]}]
set_property -dict {PACKAGE_PIN M22 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[5]}]
set_property -dict {PACKAGE_PIN R23 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[6]}]
set_property -dict {PACKAGE_PIN P23 IOSTANDARD LVCMOS18} [get_ports {gpio_bd[7]}]
set_property -dict {PACKAGE_PIN AN16 IOSTANDARD LVCMOS12 DRIVE 8} [get_ports {gpio_bd[8]}]
set_property -dict {PACKAGE_PIN AN19 IOSTANDARD LVCMOS12 DRIVE 8} [get_ports {gpio_bd[9]}]
set_property -dict {PACKAGE_PIN AP18 IOSTANDARD LVCMOS12 DRIVE 8} [get_ports {gpio_bd[10]}]
set_property -dict {PACKAGE_PIN AN14 IOSTANDARD LVCMOS12 DRIVE 8} [get_ports {gpio_bd[11]}]
set_property -dict {PACKAGE_PIN AD10 IOSTANDARD LVCMOS18 DRIVE 8} [get_ports {gpio_bd[12]}]
set_property -dict {PACKAGE_PIN AE8 IOSTANDARD LVCMOS18 DRIVE 8} [get_ports {gpio_bd[13]}]
set_property -dict {PACKAGE_PIN AF8 IOSTANDARD LVCMOS18 DRIVE 8} [get_ports {gpio_bd[14]}]
set_property -dict {PACKAGE_PIN AF9 IOSTANDARD LVCMOS18 DRIVE 8} [get_ports {gpio_bd[15]}]
set_property -dict {PACKAGE_PIN AE10 IOSTANDARD LVCMOS18 DRIVE 8} [get_ports {gpio_bd[16]}]

# ddr
set_property -dict {PACKAGE_PIN AK17} [get_ports sys_clk_p]
set_property -dict {PACKAGE_PIN AK16} [get_ports sys_clk_n]

set_property INTERNAL_VREF 0.84 [get_iobanks 44]
set_property INTERNAL_VREF 0.84 [get_iobanks 45]
set_property INTERNAL_VREF 0.84 [get_iobanks 46]

# daq2
set_property -dict {PACKAGE_PIN K6} [get_ports tx_ref_clk_p]
set_property -dict {PACKAGE_PIN K5} [get_ports tx_ref_clk_n]
set_property -dict {PACKAGE_PIN H6} [get_ports rx_ref_clk_p]
set_property -dict {PACKAGE_PIN H5} [get_ports rx_ref_clk_n]

set_property -dict {PACKAGE_PIN G9 IOSTANDARD LVDS} [get_ports rx_sync_p]
set_property -dict {PACKAGE_PIN F9 IOSTANDARD LVDS} [get_ports rx_sync_n]
set_property -dict {PACKAGE_PIN A13 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports rx_sysref_p]
set_property -dict {PACKAGE_PIN A12 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports rx_sysref_n]
set_property -dict {PACKAGE_PIN K10 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports tx_sync_p]
set_property -dict {PACKAGE_PIN J10 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports tx_sync_n]
set_property -dict {PACKAGE_PIN L12 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports tx_sysref_p]
set_property -dict {PACKAGE_PIN K12 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports tx_sysref_n]

set_property -dict {PACKAGE_PIN L13 IOSTANDARD LVCMOS18} [get_ports spi_csn_clk]
set_property -dict {PACKAGE_PIN L8 IOSTANDARD LVCMOS18} [get_ports spi_csn_dac]
set_property -dict {PACKAGE_PIN H9 IOSTANDARD LVCMOS18} [get_ports spi_csn_adc]
set_property -dict {PACKAGE_PIN K13 IOSTANDARD LVCMOS18} [get_ports spi_clk]
set_property -dict {PACKAGE_PIN J9 IOSTANDARD LVCMOS18} [get_ports spi_sdio]
set_property -dict {PACKAGE_PIN H8 IOSTANDARD LVCMOS18} [get_ports spi_dir]

set_property -dict {PACKAGE_PIN J8 IOSTANDARD LVCMOS18} [get_ports clkd_sync]
set_property -dict {PACKAGE_PIN K8 IOSTANDARD LVCMOS18} [get_ports dac_reset]
set_property -dict {PACKAGE_PIN D10 IOSTANDARD LVCMOS18} [get_ports dac_txen]
set_property -dict {PACKAGE_PIN D13 IOSTANDARD LVCMOS18} [get_ports adc_pd]

set_property -dict {PACKAGE_PIN D9 IOSTANDARD LVCMOS18} [get_ports {clkd_status[0]}]
set_property -dict {PACKAGE_PIN C9 IOSTANDARD LVCMOS18} [get_ports {clkd_status[1]}]
set_property -dict {PACKAGE_PIN E10 IOSTANDARD LVCMOS18} [get_ports dac_irq]
set_property -dict {PACKAGE_PIN K11 IOSTANDARD LVCMOS18} [get_ports adc_fda]
set_property -dict {PACKAGE_PIN J11 IOSTANDARD LVCMOS18} [get_ports adc_fdb]

set_property -dict {PACKAGE_PIN F8 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports trig_p]
set_property -dict {PACKAGE_PIN E8 IOSTANDARD LVDS DIFF_TERM_ADV TERM_100} [get_ports trig_n]

## clocks

create_clock -period 2.000 -name tx_ref_clk [get_ports tx_ref_clk_p]
create_clock -period 2.000 -name rx_ref_clk [get_ports rx_ref_clk_p]
create_clock -period 4.000 -name tx_div_clk [get_pins u_system_wrapper/system_i/util_daq2_xcvr/inst/i_xch_0/i_gthe3_channel/TXOUTCLK]
create_clock -period 4.000 -name rx_div_clk [get_pins u_system_wrapper/system_i/util_daq2_xcvr/inst/i_xch_0/i_gthe3_channel/RXOUTCLK]

## gt pin assignments below are for reference only and are ignored by the tool!

###  set_property  -dict {PACKAGE_PIN  A4} [get_ports rx_data_p[0]] ; ## A10  FMC_HPC_DP3_M2C_P
###  set_property  -dict {PACKAGE_PIN  A3} [get_ports rx_data_n[0]] ; ## A11  FMC_HPC_DP3_M2C_N
###  set_property  -dict {PACKAGE_PIN  E4} [get_ports rx_data_p[1]] ; ## C06  FMC_HPC_DP0_M2C_P
###  set_property  -dict {PACKAGE_PIN  E3} [get_ports rx_data_n[1]] ; ## C07  FMC_HPC_DP0_M2C_N
###  set_property  -dict {PACKAGE_PIN  B2} [get_ports rx_data_p[2]] ; ## A06  FMC_HPC_DP2_M2C_P
###  set_property  -dict {PACKAGE_PIN  B1} [get_ports rx_data_n[2]] ; ## A07  FMC_HPC_DP2_M2C_N
###  set_property  -dict {PACKAGE_PIN  D2} [get_ports rx_data_p[3]] ; ## A02  FMC_HPC_DP1_M2C_P
###  set_property  -dict {PACKAGE_PIN  D1} [get_ports rx_data_n[3]] ; ## A03  FMC_HPC_DP1_M2C_N
###  set_property  -dict {PACKAGE_PIN  B6} [get_ports tx_data_p[0]] ; ## A30  FMC_HPC_DP3_C2M_P (tx_data_p[0])
###  set_property  -dict {PACKAGE_PIN  B5} [get_ports tx_data_n[0]] ; ## A31  FMC_HPC_DP3_C2M_N (tx_data_n[0])
###  set_property  -dict {PACKAGE_PIN  F6} [get_ports tx_data_p[1]] ; ## C02  FMC_HPC_DP0_C2M_P (tx_data_p[3])
###  set_property  -dict {PACKAGE_PIN  F5} [get_ports tx_data_n[1]] ; ## C03  FMC_HPC_DP0_C2M_N (tx_data_n[3])
###  set_property  -dict {PACKAGE_PIN  C4} [get_ports tx_data_p[2]] ; ## A26  FMC_HPC_DP2_C2M_P (tx_data_p[1])
###  set_property  -dict {PACKAGE_PIN  C3} [get_ports tx_data_n[2]] ; ## A27  FMC_HPC_DP2_C2M_N (tx_data_n[1])
###  set_property  -dict {PACKAGE_PIN  D6} [get_ports tx_data_p[3]] ; ## A22  FMC_HPC_DP1_C2M_P (tx_data_p[2])
###  set_property  -dict {PACKAGE_PIN  D5} [get_ports tx_data_n[3]] ; ## A23  FMC_HPC_DP1_C2M_N (tx_data_n[2])

set_property LOC GTHE3_CHANNEL_X0Y19 [get_cells -hierarchical -filter {NAME =~ *util_daq2_xcvr/inst/i_xch_0/i_gthe3_channel}]
set_property LOC GTHE3_CHANNEL_X0Y16 [get_cells -hierarchical -filter {NAME =~ *util_daq2_xcvr/inst/i_xch_1/i_gthe3_channel}]
set_property LOC GTHE3_CHANNEL_X0Y18 [get_cells -hierarchical -filter {NAME =~ *util_daq2_xcvr/inst/i_xch_2/i_gthe3_channel}]
set_property LOC GTHE3_CHANNEL_X0Y17 [get_cells -hierarchical -filter {NAME =~ *util_daq2_xcvr/inst/i_xch_3/i_gthe3_channel}]




#create_debug_core u_ila_0 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
#set_property C_DATA_DEPTH 4096 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 2 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property port_width 1 [get_debug_ports u_ila_0/clk]
#connect_debug_port u_ila_0/clk [get_nets [list xillybus_ins/pcie/inst/gt_top_i/phy_clk_i/CLK_USERCLK]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#set_property port_width 64 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {dac_data_0[0]} {dac_data_0[1]} {dac_data_0[2]} {dac_data_0[3]} {dac_data_0[4]} {dac_data_0[5]} {dac_data_0[6]} {dac_data_0[7]} {dac_data_0[8]} {dac_data_0[9]} {dac_data_0[10]} {dac_data_0[11]} {dac_data_0[12]} {dac_data_0[13]} {dac_data_0[14]} {dac_data_0[15]} {dac_data_0[16]} {dac_data_0[17]} {dac_data_0[18]} {dac_data_0[19]} {dac_data_0[20]} {dac_data_0[21]} {dac_data_0[22]} {dac_data_0[23]} {dac_data_0[24]} {dac_data_0[25]} {dac_data_0[26]} {dac_data_0[27]} {dac_data_0[28]} {dac_data_0[29]} {dac_data_0[30]} {dac_data_0[31]} {dac_data_0[32]} {dac_data_0[33]} {dac_data_0[34]} {dac_data_0[35]} {dac_data_0[36]} {dac_data_0[37]} {dac_data_0[38]} {dac_data_0[39]} {dac_data_0[40]} {dac_data_0[41]} {dac_data_0[42]} {dac_data_0[43]} {dac_data_0[44]} {dac_data_0[45]} {dac_data_0[46]} {dac_data_0[47]} {dac_data_0[48]} {dac_data_0[49]} {dac_data_0[50]} {dac_data_0[51]} {dac_data_0[52]} {dac_data_0[53]} {dac_data_0[54]} {dac_data_0[55]} {dac_data_0[56]} {dac_data_0[57]} {dac_data_0[58]} {dac_data_0[59]} {dac_data_0[60]} {dac_data_0[61]} {dac_data_0[62]} {dac_data_0[63]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
#set_property port_width 3 [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list {tx_prog_full_dac[0]} {tx_prog_full_dac[1]} {tx_prog_full_dac[2]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
#set_property port_width 128 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list {user_w_write_128_data[0]} {user_w_write_128_data[1]} {user_w_write_128_data[2]} {user_w_write_128_data[3]} {user_w_write_128_data[4]} {user_w_write_128_data[5]} {user_w_write_128_data[6]} {user_w_write_128_data[7]} {user_w_write_128_data[8]} {user_w_write_128_data[9]} {user_w_write_128_data[10]} {user_w_write_128_data[11]} {user_w_write_128_data[12]} {user_w_write_128_data[13]} {user_w_write_128_data[14]} {user_w_write_128_data[15]} {user_w_write_128_data[16]} {user_w_write_128_data[17]} {user_w_write_128_data[18]} {user_w_write_128_data[19]} {user_w_write_128_data[20]} {user_w_write_128_data[21]} {user_w_write_128_data[22]} {user_w_write_128_data[23]} {user_w_write_128_data[24]} {user_w_write_128_data[25]} {user_w_write_128_data[26]} {user_w_write_128_data[27]} {user_w_write_128_data[28]} {user_w_write_128_data[29]} {user_w_write_128_data[30]} {user_w_write_128_data[31]} {user_w_write_128_data[32]} {user_w_write_128_data[33]} {user_w_write_128_data[34]} {user_w_write_128_data[35]} {user_w_write_128_data[36]} {user_w_write_128_data[37]} {user_w_write_128_data[38]} {user_w_write_128_data[39]} {user_w_write_128_data[40]} {user_w_write_128_data[41]} {user_w_write_128_data[42]} {user_w_write_128_data[43]} {user_w_write_128_data[44]} {user_w_write_128_data[45]} {user_w_write_128_data[46]} {user_w_write_128_data[47]} {user_w_write_128_data[48]} {user_w_write_128_data[49]} {user_w_write_128_data[50]} {user_w_write_128_data[51]} {user_w_write_128_data[52]} {user_w_write_128_data[53]} {user_w_write_128_data[54]} {user_w_write_128_data[55]} {user_w_write_128_data[56]} {user_w_write_128_data[57]} {user_w_write_128_data[58]} {user_w_write_128_data[59]} {user_w_write_128_data[60]} {user_w_write_128_data[61]} {user_w_write_128_data[62]} {user_w_write_128_data[63]} {user_w_write_128_data[64]} {user_w_write_128_data[65]} {user_w_write_128_data[66]} {user_w_write_128_data[67]} {user_w_write_128_data[68]} {user_w_write_128_data[69]} {user_w_write_128_data[70]} {user_w_write_128_data[71]} {user_w_write_128_data[72]} {user_w_write_128_data[73]} {user_w_write_128_data[74]} {user_w_write_128_data[75]} {user_w_write_128_data[76]} {user_w_write_128_data[77]} {user_w_write_128_data[78]} {user_w_write_128_data[79]} {user_w_write_128_data[80]} {user_w_write_128_data[81]} {user_w_write_128_data[82]} {user_w_write_128_data[83]} {user_w_write_128_data[84]} {user_w_write_128_data[85]} {user_w_write_128_data[86]} {user_w_write_128_data[87]} {user_w_write_128_data[88]} {user_w_write_128_data[89]} {user_w_write_128_data[90]} {user_w_write_128_data[91]} {user_w_write_128_data[92]} {user_w_write_128_data[93]} {user_w_write_128_data[94]} {user_w_write_128_data[95]} {user_w_write_128_data[96]} {user_w_write_128_data[97]} {user_w_write_128_data[98]} {user_w_write_128_data[99]} {user_w_write_128_data[100]} {user_w_write_128_data[101]} {user_w_write_128_data[102]} {user_w_write_128_data[103]} {user_w_write_128_data[104]} {user_w_write_128_data[105]} {user_w_write_128_data[106]} {user_w_write_128_data[107]} {user_w_write_128_data[108]} {user_w_write_128_data[109]} {user_w_write_128_data[110]} {user_w_write_128_data[111]} {user_w_write_128_data[112]} {user_w_write_128_data[113]} {user_w_write_128_data[114]} {user_w_write_128_data[115]} {user_w_write_128_data[116]} {user_w_write_128_data[117]} {user_w_write_128_data[118]} {user_w_write_128_data[119]} {user_w_write_128_data[120]} {user_w_write_128_data[121]} {user_w_write_128_data[122]} {user_w_write_128_data[123]} {user_w_write_128_data[124]} {user_w_write_128_data[125]} {user_w_write_128_data[126]} {user_w_write_128_data[127]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
#set_property port_width 11 [get_debug_ports u_ila_0/probe3]
#connect_debug_port u_ila_0/probe3 [get_nets [list {tx_wr_data_count[0]} {tx_wr_data_count[1]} {tx_wr_data_count[2]} {tx_wr_data_count[3]} {tx_wr_data_count[4]} {tx_wr_data_count[5]} {tx_wr_data_count[6]} {tx_wr_data_count[7]} {tx_wr_data_count[8]} {tx_wr_data_count[9]} {tx_wr_data_count[10]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
#set_property port_width 128 [get_debug_ports u_ila_0/probe4]
#connect_debug_port u_ila_0/probe4 [get_nets [list {dac_data[0]} {dac_data[1]} {dac_data[2]} {dac_data[3]} {dac_data[4]} {dac_data[5]} {dac_data[6]} {dac_data[7]} {dac_data[8]} {dac_data[9]} {dac_data[10]} {dac_data[11]} {dac_data[12]} {dac_data[13]} {dac_data[14]} {dac_data[15]} {dac_data[16]} {dac_data[17]} {dac_data[18]} {dac_data[19]} {dac_data[20]} {dac_data[21]} {dac_data[22]} {dac_data[23]} {dac_data[24]} {dac_data[25]} {dac_data[26]} {dac_data[27]} {dac_data[28]} {dac_data[29]} {dac_data[30]} {dac_data[31]} {dac_data[32]} {dac_data[33]} {dac_data[34]} {dac_data[35]} {dac_data[36]} {dac_data[37]} {dac_data[38]} {dac_data[39]} {dac_data[40]} {dac_data[41]} {dac_data[42]} {dac_data[43]} {dac_data[44]} {dac_data[45]} {dac_data[46]} {dac_data[47]} {dac_data[48]} {dac_data[49]} {dac_data[50]} {dac_data[51]} {dac_data[52]} {dac_data[53]} {dac_data[54]} {dac_data[55]} {dac_data[56]} {dac_data[57]} {dac_data[58]} {dac_data[59]} {dac_data[60]} {dac_data[61]} {dac_data[62]} {dac_data[63]} {dac_data[64]} {dac_data[65]} {dac_data[66]} {dac_data[67]} {dac_data[68]} {dac_data[69]} {dac_data[70]} {dac_data[71]} {dac_data[72]} {dac_data[73]} {dac_data[74]} {dac_data[75]} {dac_data[76]} {dac_data[77]} {dac_data[78]} {dac_data[79]} {dac_data[80]} {dac_data[81]} {dac_data[82]} {dac_data[83]} {dac_data[84]} {dac_data[85]} {dac_data[86]} {dac_data[87]} {dac_data[88]} {dac_data[89]} {dac_data[90]} {dac_data[91]} {dac_data[92]} {dac_data[93]} {dac_data[94]} {dac_data[95]} {dac_data[96]} {dac_data[97]} {dac_data[98]} {dac_data[99]} {dac_data[100]} {dac_data[101]} {dac_data[102]} {dac_data[103]} {dac_data[104]} {dac_data[105]} {dac_data[106]} {dac_data[107]} {dac_data[108]} {dac_data[109]} {dac_data[110]} {dac_data[111]} {dac_data[112]} {dac_data[113]} {dac_data[114]} {dac_data[115]} {dac_data[116]} {dac_data[117]} {dac_data[118]} {dac_data[119]} {dac_data[120]} {dac_data[121]} {dac_data[122]} {dac_data[123]} {dac_data[124]} {dac_data[125]} {dac_data[126]} {dac_data[127]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
#set_property port_width 1 [get_debug_ports u_ila_0/probe5]
#connect_debug_port u_ila_0/probe5 [get_nets [list dac_en]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
#set_property port_width 1 [get_debug_ports u_ila_0/probe6]
#connect_debug_port u_ila_0/probe6 [get_nets [list dac_enable_0]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
#set_property port_width 1 [get_debug_ports u_ila_0/probe7]
#connect_debug_port u_ila_0/probe7 [get_nets [list dac_rd_en]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
#set_property port_width 1 [get_debug_ports u_ila_0/probe8]
#connect_debug_port u_ila_0/probe8 [get_nets [list dac_sync]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
#set_property port_width 1 [get_debug_ports u_ila_0/probe9]
#connect_debug_port u_ila_0/probe9 [get_nets [list dac_valid_0]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
#set_property port_width 1 [get_debug_ports u_ila_0/probe10]
#connect_debug_port u_ila_0/probe10 [get_nets [list dac_valid_1]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
#set_property port_width 1 [get_debug_ports u_ila_0/probe11]
#connect_debug_port u_ila_0/probe11 [get_nets [list reg_start]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
#set_property port_width 1 [get_debug_ports u_ila_0/probe12]
#connect_debug_port u_ila_0/probe12 [get_nets [list tx_empty]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
#set_property port_width 1 [get_debug_ports u_ila_0/probe13]
#connect_debug_port u_ila_0/probe13 [get_nets [list tx_empty_d]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
#set_property port_width 1 [get_debug_ports u_ila_0/probe14]
#connect_debug_port u_ila_0/probe14 [get_nets [list tx_empty_r]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
#set_property port_width 1 [get_debug_ports u_ila_0/probe15]
#connect_debug_port u_ila_0/probe15 [get_nets [list tx_prog_full]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
#set_property port_width 1 [get_debug_ports u_ila_0/probe16]
#connect_debug_port u_ila_0/probe16 [get_nets [list user_r_read_128_empty]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
#set_property port_width 1 [get_debug_ports u_ila_0/probe17]
#connect_debug_port u_ila_0/probe17 [get_nets [list user_r_read_128_open]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe18]
#set_property port_width 1 [get_debug_ports u_ila_0/probe18]
#connect_debug_port u_ila_0/probe18 [get_nets [list user_r_read_128_rden]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe19]
#set_property port_width 1 [get_debug_ports u_ila_0/probe19]
#connect_debug_port u_ila_0/probe19 [get_nets [list user_w_write_128_full]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe20]
#set_property port_width 1 [get_debug_ports u_ila_0/probe20]
#connect_debug_port u_ila_0/probe20 [get_nets [list user_w_write_128_open]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe21]
#set_property port_width 1 [get_debug_ports u_ila_0/probe21]
#connect_debug_port u_ila_0/probe21 [get_nets [list user_w_write_128_wren]]
#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets clk]
