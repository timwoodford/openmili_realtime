//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.2.1 (win64) Build 2288692 Thu Jul 26 18:24:02 MDT 2018
//Date        : Tue Feb 26 19:06:19 2019
//Host        : DESKTOP-IT65841 running 64-bit major release  (build 9200)
//Command     : generate_target system_wrapper.bd
//Design      : system_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_wrapper
   (adc_clk,
    adc_data,
    adc_wr_en,
    c0_ddr4_act_n,
    c0_ddr4_adr,
    c0_ddr4_ba,
    c0_ddr4_bg,
    c0_ddr4_ck_c,
    c0_ddr4_ck_t,
    c0_ddr4_cke,
    c0_ddr4_cs_n,
    c0_ddr4_dm_n,
    c0_ddr4_dq,
    c0_ddr4_dqs_c,
    c0_ddr4_dqs_t,
    c0_ddr4_odt,
    c0_ddr4_reset_n,
    dac_clk,
    dac_data,
    dac_data_0,
    dac_data_1,
    dac_dunf,
    dac_rd_en,
    dac_rst,
    dac_valid_0,
    dac_valid_1,
    gpio0_i,
    gpio0_o,
    gpio0_t,
    gpio1_i,
    gpio1_o,
    gpio1_t,
    mb_intr_4,
    mb_intr_5,
    rx_data_0_n,
    rx_data_0_p,
    rx_data_1_n,
    rx_data_1_p,
    rx_data_2_n,
    rx_data_2_p,
    rx_data_3_n,
    rx_data_3_p,
    rx_ref_clk_0,
    rx_sync_0,
    rx_sysref_0,
    spi_clk_i,
    spi_clk_o,
    spi_csn_i,
    spi_csn_o,
    spi_sdi_i,
    spi_sdo_i,
    spi_sdo_o,
    sys_clk_clk_n,
    sys_clk_clk_p,
    sys_rst,
    tx_data_0_n,
    tx_data_0_p,
    tx_data_1_n,
    tx_data_1_p,
    tx_data_2_n,
    tx_data_2_p,
    tx_data_3_n,
    tx_data_3_p,
    tx_ref_clk_0,
    tx_sync_0,
    tx_sysref_0,
    uart_sin,
    uart_sout);
  output adc_clk;
  output [127:0]adc_data;
  output adc_wr_en;
  output c0_ddr4_act_n;
  output [16:0]c0_ddr4_adr;
  output [1:0]c0_ddr4_ba;
  output c0_ddr4_bg;
  output c0_ddr4_ck_c;
  output c0_ddr4_ck_t;
  output c0_ddr4_cke;
  output c0_ddr4_cs_n;
  inout [7:0]c0_ddr4_dm_n;
  inout [63:0]c0_ddr4_dq;
  inout [7:0]c0_ddr4_dqs_c;
  inout [7:0]c0_ddr4_dqs_t;
  output c0_ddr4_odt;
  output c0_ddr4_reset_n;
  output dac_clk;
  input [127:0]dac_data;
  output [63:0]dac_data_0;
  output [63:0]dac_data_1;
  input dac_dunf;
  output dac_rd_en;
  output [0:0]dac_rst;
  output dac_valid_0;
  output dac_valid_1;
  input [31:0]gpio0_i;
  output [31:0]gpio0_o;
  output [31:0]gpio0_t;
  input [31:0]gpio1_i;
  output [31:0]gpio1_o;
  output [31:0]gpio1_t;
  input mb_intr_4;
  input mb_intr_5;
  input rx_data_0_n;
  input rx_data_0_p;
  input rx_data_1_n;
  input rx_data_1_p;
  input rx_data_2_n;
  input rx_data_2_p;
  input rx_data_3_n;
  input rx_data_3_p;
  input rx_ref_clk_0;
  output [0:0]rx_sync_0;
  input rx_sysref_0;
  input spi_clk_i;
  output spi_clk_o;
  input [7:0]spi_csn_i;
  output [7:0]spi_csn_o;
  input spi_sdi_i;
  input spi_sdo_i;
  output spi_sdo_o;
  input sys_clk_clk_n;
  input sys_clk_clk_p;
  input sys_rst;
  output tx_data_0_n;
  output tx_data_0_p;
  output tx_data_1_n;
  output tx_data_1_p;
  output tx_data_2_n;
  output tx_data_2_p;
  output tx_data_3_n;
  output tx_data_3_p;
  input tx_ref_clk_0;
  input tx_sync_0;
  input tx_sysref_0;
  input uart_sin;
  output uart_sout;

  wire adc_clk;
  wire [127:0]adc_data;
  wire adc_wr_en;
  wire c0_ddr4_act_n;
  wire [16:0]c0_ddr4_adr;
  wire [1:0]c0_ddr4_ba;
  wire c0_ddr4_bg;
  wire c0_ddr4_ck_c;
  wire c0_ddr4_ck_t;
  wire c0_ddr4_cke;
  wire c0_ddr4_cs_n;
  wire [7:0]c0_ddr4_dm_n;
  wire [63:0]c0_ddr4_dq;
  wire [7:0]c0_ddr4_dqs_c;
  wire [7:0]c0_ddr4_dqs_t;
  wire c0_ddr4_odt;
  wire c0_ddr4_reset_n;
  wire dac_clk;
  wire [127:0]dac_data;
  wire [63:0]dac_data_0;
  wire [63:0]dac_data_1;
  wire dac_dunf;
  wire dac_rd_en;
  wire [0:0]dac_rst;
  wire dac_valid_0;
  wire dac_valid_1;
  wire [31:0]gpio0_i;
  wire [31:0]gpio0_o;
  wire [31:0]gpio0_t;
  wire [31:0]gpio1_i;
  wire [31:0]gpio1_o;
  wire [31:0]gpio1_t;
  wire mb_intr_4;
  wire mb_intr_5;
  wire rx_data_0_n;
  wire rx_data_0_p;
  wire rx_data_1_n;
  wire rx_data_1_p;
  wire rx_data_2_n;
  wire rx_data_2_p;
  wire rx_data_3_n;
  wire rx_data_3_p;
  wire rx_ref_clk_0;
  wire [0:0]rx_sync_0;
  wire rx_sysref_0;
  wire spi_clk_i;
  wire spi_clk_o;
  wire [7:0]spi_csn_i;
  wire [7:0]spi_csn_o;
  wire spi_sdi_i;
  wire spi_sdo_i;
  wire spi_sdo_o;
  wire sys_clk_clk_n;
  wire sys_clk_clk_p;
  wire sys_rst;
  wire tx_data_0_n;
  wire tx_data_0_p;
  wire tx_data_1_n;
  wire tx_data_1_p;
  wire tx_data_2_n;
  wire tx_data_2_p;
  wire tx_data_3_n;
  wire tx_data_3_p;
  wire tx_ref_clk_0;
  wire tx_sync_0;
  wire tx_sysref_0;
  wire uart_sin;
  wire uart_sout;

  system system_i
       (.adc_clk(adc_clk),
        .adc_data(adc_data),
        .adc_wr_en(adc_wr_en),
        .c0_ddr4_act_n(c0_ddr4_act_n),
        .c0_ddr4_adr(c0_ddr4_adr),
        .c0_ddr4_ba(c0_ddr4_ba),
        .c0_ddr4_bg(c0_ddr4_bg),
        .c0_ddr4_ck_c(c0_ddr4_ck_c),
        .c0_ddr4_ck_t(c0_ddr4_ck_t),
        .c0_ddr4_cke(c0_ddr4_cke),
        .c0_ddr4_cs_n(c0_ddr4_cs_n),
        .c0_ddr4_dm_n(c0_ddr4_dm_n),
        .c0_ddr4_dq(c0_ddr4_dq),
        .c0_ddr4_dqs_c(c0_ddr4_dqs_c),
        .c0_ddr4_dqs_t(c0_ddr4_dqs_t),
        .c0_ddr4_odt(c0_ddr4_odt),
        .c0_ddr4_reset_n(c0_ddr4_reset_n),
        .dac_clk(dac_clk),
        .dac_data(dac_data),
        .dac_data_0(dac_data_0),
        .dac_data_1(dac_data_1),
        .dac_dunf(dac_dunf),
        .dac_rd_en(dac_rd_en),
        .dac_rst(dac_rst),
        .dac_valid_0(dac_valid_0),
        .dac_valid_1(dac_valid_1),
        .gpio0_i(gpio0_i),
        .gpio0_o(gpio0_o),
        .gpio0_t(gpio0_t),
        .gpio1_i(gpio1_i),
        .gpio1_o(gpio1_o),
        .gpio1_t(gpio1_t),
        .mb_intr_4(mb_intr_4),
        .mb_intr_5(mb_intr_5),
        .rx_data_0_n(rx_data_0_n),
        .rx_data_0_p(rx_data_0_p),
        .rx_data_1_n(rx_data_1_n),
        .rx_data_1_p(rx_data_1_p),
        .rx_data_2_n(rx_data_2_n),
        .rx_data_2_p(rx_data_2_p),
        .rx_data_3_n(rx_data_3_n),
        .rx_data_3_p(rx_data_3_p),
        .rx_ref_clk_0(rx_ref_clk_0),
        .rx_sync_0(rx_sync_0),
        .rx_sysref_0(rx_sysref_0),
        .spi_clk_i(spi_clk_i),
        .spi_clk_o(spi_clk_o),
        .spi_csn_i(spi_csn_i),
        .spi_csn_o(spi_csn_o),
        .spi_sdi_i(spi_sdi_i),
        .spi_sdo_i(spi_sdo_i),
        .spi_sdo_o(spi_sdo_o),
        .sys_clk_clk_n(sys_clk_clk_n),
        .sys_clk_clk_p(sys_clk_clk_p),
        .sys_rst(sys_rst),
        .tx_data_0_n(tx_data_0_n),
        .tx_data_0_p(tx_data_0_p),
        .tx_data_1_n(tx_data_1_n),
        .tx_data_1_p(tx_data_1_p),
        .tx_data_2_n(tx_data_2_n),
        .tx_data_2_p(tx_data_2_p),
        .tx_data_3_n(tx_data_3_n),
        .tx_data_3_p(tx_data_3_p),
        .tx_ref_clk_0(tx_ref_clk_0),
        .tx_sync_0(tx_sync_0),
        .tx_sysref_0(tx_sysref_0),
        .uart_sin(uart_sin),
        .uart_sout(uart_sout));
endmodule
