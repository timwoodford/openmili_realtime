/*
 * openmili.c
 *
 *  Created on: Jul 17, 2018
 *      Author: fencerex
 */

#include "platform_drivers.h"
#include "ad9523.h"
#include "ad9680.h"
#include "ad9144.h"
#include "adc_core.h"
#include "dac_core.h"
#include "dmac_core.h"
#include "dac_buffer.h"
#include "xcvr_core.h"
#include "jesd_core.h"

#define GPIO_TRIG		    43
#define GPIO_ADC_PD		    42
#define GPIO_DAC_TXEN		41
#define GPIO_DAC_RESET		40
#define GPIO_CLKD_SYNC		38
#define GPIO_ADC_FDB		36
#define GPIO_ADC_FDA		35
#define GPIO_DAC_IRQ		34
#define GPIO_CLKD_STATUS_1	33
#define GPIO_CLKD_STATUS_0	32

#define DMA_BUFFER		 0

enum ad9523_channels {
	DAC_DEVICE_CLK,
	DAC_DEVICE_SYSREF,
	DAC_FPGA_CLK,
	DAC_FPGA_SYSREF,
	ADC_DEVICE_CLK,
	ADC_DEVICE_SYSREF,
	ADC_FPGA_CLK,
	ADC_FPGA_SYSREF
};



int main(void)
{
	dac_core		ad9144_core;
	dac_channel		ad9144_channels[2];
	jesd_core		ad9144_jesd;
	dmac_core		ad9144_dma;
	xcvr_core		ad9144_xcvr;
	adc_core		ad9680_core;
	jesd_core		ad9680_jesd;
	xcvr_core		ad9680_xcvr;
	dmac_core       ad9680_dma;
	dmac_xfer       rx_xfer;
	dmac_xfer       tx_xfer;

	printf("Start Configuration\n");
	//Set Up SPI protocols
	spi_init_param	ad9523_spi_param;
	spi_init_param  ad9144_spi_param;
	spi_init_param  ad9680_spi_param;

	ad9523_spi_param.type = MICROBLAZE_SPI;
	ad9523_spi_param.chip_select = 0x6;
	ad9523_spi_param.cpha = 0;
	ad9523_spi_param.cpol = 0;

	ad9144_spi_param.type = MICROBLAZE_SPI;
	ad9144_spi_param.chip_select = 0x5;
	ad9144_spi_param.cpha = 0;
	ad9144_spi_param.cpol = 0;

	ad9680_spi_param.type = MICROBLAZE_SPI;
	ad9680_spi_param.chip_select = 0x3;
	ad9680_spi_param.cpha = 0;
	ad9680_spi_param.cpol = 0;

	struct ad9523_channel_spec	ad9523_channels[8];
	struct ad9523_platform_data	ad9523_pdata;
	struct ad9523_init_param	ad9523_param;
	struct ad9144_init_param	ad9144_param;
	struct ad9680_init_param	ad9680_param;

	ad9523_param.spi_init = ad9523_spi_param;
	ad9144_param.spi_init = ad9144_spi_param;
	ad9680_param.spi_init = ad9680_spi_param;

	struct ad9523_dev *ad9523_device;
	struct ad9144_dev *ad9144_device;
	struct ad9680_dev *ad9680_device;


	ad9144_xcvr.base_address = XPAR_AXI_AD9144_XCVR_BASEADDR;
	ad9144_core.base_address = XPAR_AXI_AD9144_CORE_BASEADDR;
	ad9144_jesd.base_address = XPAR_AXI_AD9144_JESD_TX_AXI_BASEADDR;
	ad9144_dma.base_address = XPAR_AXI_AD9144_DMA_BASEADDR;
	ad9680_xcvr.base_address = XPAR_AXI_AD9680_XCVR_BASEADDR;
	ad9680_core.base_address = XPAR_AXI_AD9680_CORE_BASEADDR;
	ad9680_jesd.base_address = XPAR_AXI_AD9680_JESD_RX_AXI_BASEADDR;
	ad9680_dma.base_address = XPAR_AXI_AD9680_DMA_BASEADDR;

	rx_xfer.start_address = XPAR_AXI_DDR_CNTRL_BASEADDR + 0x800000;
	tx_xfer.start_address = XPAR_AXI_DDR_CNTRL_BASEADDR + 0x900000;



	ad9523_pdata.num_channels = 8;
	ad9523_pdata.channels = &ad9523_channels[0];
	ad9523_param.pdata = &ad9523_pdata;
	ad9523_init(&ad9523_param);

	ad9523_channels[DAC_DEVICE_CLK].channel_num = 1;
	ad9523_channels[DAC_DEVICE_CLK].channel_divider = 1;
	ad9523_channels[DAC_DEVICE_SYSREF].channel_num = 7;
	ad9523_channels[DAC_DEVICE_SYSREF].channel_divider = 128;
	ad9523_channels[DAC_FPGA_CLK].channel_num = 9;
	ad9523_channels[DAC_FPGA_CLK].channel_divider = 2;
	ad9523_channels[DAC_FPGA_SYSREF].channel_num = 8;
	ad9523_channels[DAC_FPGA_SYSREF].channel_divider = 128;

	ad9523_channels[ADC_DEVICE_CLK].channel_num = 13;
	ad9523_channels[ADC_DEVICE_CLK].channel_divider = 1;
	ad9523_channels[ADC_DEVICE_SYSREF].channel_num = 6;
	ad9523_channels[ADC_DEVICE_SYSREF].channel_divider = 128;
	ad9523_channels[ADC_FPGA_CLK].channel_num = 4;
	ad9523_channels[ADC_FPGA_CLK].channel_divider = 2;
	ad9523_channels[ADC_FPGA_SYSREF].channel_num = 5;
	ad9523_channels[ADC_FPGA_SYSREF].channel_divider = 128;

	// VCXO 125MHz

	ad9523_pdata.vcxo_freq = 125000000;
	ad9523_pdata.spi3wire = 1;
	ad9523_pdata.osc_in_diff_en = 1;
	ad9523_pdata.pll2_charge_pump_current_nA = 413000;
	ad9523_pdata.pll2_freq_doubler_en = 0;
	ad9523_pdata.pll2_r2_div = 1;
	ad9523_pdata.pll2_ndiv_a_cnt = 0;
	ad9523_pdata.pll2_ndiv_b_cnt = 6;
	ad9523_pdata.pll2_vco_diff_m1 = 3;
	ad9523_pdata.pll2_vco_diff_m2 = 0;
	ad9523_pdata.rpole2 = 0;
	ad9523_pdata.rzero = 7;
	ad9523_pdata.cpole1 = 2;

	ad9144_xcvr.ref_clock_khz = 500000;
	ad9680_xcvr.ref_clock_khz = 500000;
	//******************************************************************************
	// bring up the system
	//******************************************************************************

	// setup GPIOs

	gpio_desc *clkd_sync;
	gpio_desc *dac_reset;
	gpio_desc *dac_txen;
	gpio_desc *adc_pd;

	gpio_get(&clkd_sync, GPIO_CLKD_SYNC);
	gpio_get(&dac_reset, GPIO_DAC_RESET);
	gpio_get(&dac_txen,  GPIO_DAC_TXEN);
	gpio_get(&adc_pd,    GPIO_ADC_PD);

	gpio_set_value(clkd_sync, 0);
	gpio_set_value(dac_reset, 0);
	gpio_set_value(dac_txen,  0);
	gpio_set_value(adc_pd,    1);
	mdelay(5);

	gpio_set_value(clkd_sync, 1);
	gpio_set_value(dac_reset, 1);
	gpio_set_value(dac_txen,  1);
	gpio_set_value(adc_pd,    0);

	// setup clocks
	ad9523_setup(&ad9523_device, ad9523_param);

	ad9523_remove(ad9523_device);

	gpio_remove(clkd_sync);
	gpio_remove(dac_reset);
	gpio_remove(dac_txen);
	gpio_remove(adc_pd);
	printf("It is Finished\n");

	return(0);
}
