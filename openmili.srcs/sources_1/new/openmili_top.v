`timescale 1ns / 100ps

/*****************************************************************
 * Project    : openmili
 * FPGA       : KCU105 Dev. Board xcku040-ffva1156-2-e
 * File       : openmili_top.v
 * Last Edit  : Time-stamp: <2018-05-11 15:19:55 platt>
 * Description: Top Level Instantiation 
 *              Modified from ADI DAQ2 example design
 *              This project is heavily based off of ADI's example project
 *              Removed and rearranged some parts of the HDL to be more user friendly
 *              
 *  
 /*****************************************************************/

module openmili_top
  (  
     input         sys_rst,
     input         sys_clk_p,
     input         sys_clk_n,

     //UART
     input         uart_sin,
     output        uart_sout,

     //DDR4
     output        ddr4_act_n,
     output [16:0] ddr4_addr,
     output [1:0]  ddr4_ba,
     output [0:0]  ddr4_bg,
     output        ddr4_ck_p,
     output        ddr4_ck_n,
     output [0:0]  ddr4_cke,
     output [0:0]  ddr4_cs_n,
     inout [7:0]   ddr4_dm_n,
     inout [63:0]  ddr4_dq,
     inout [7:0]   ddr4_dqs_p,
     inout [7:0]   ddr4_dqs_n,
     output [0:0]  ddr4_odt,
     output        ddr4_reset_n,

     //Generic GPIO
     inout [16:4]  gpio_bd,

     //ADC
     input         rx_ref_clk_p,
     input         rx_ref_clk_n,
     input         rx_sysref_p,
     input         rx_sysref_n,
     output        rx_sync_p,
     output        rx_sync_n,
     input [ 3:0]  rx_data_p,
     input [ 3:0]  rx_data_n,

     //DAC
     input         tx_ref_clk_p,
     input         tx_ref_clk_n,
     input         tx_sysref_p,
     input         tx_sysref_n,
     input         tx_sync_p,
     input         tx_sync_n,
     output [ 3:0] tx_data_p,
     output [ 3:0] tx_data_n,

     //Trigger
     input         trig_p,
     input         trig_n,

     //DAQ2 Signals 
     inout         adc_fdb,
     inout         adc_fda,
     inout         dac_irq,
     inout [ 1:0]  clkd_status,
     inout         adc_pd,
     inout         dac_txen,
     inout         dac_reset,
     inout         clkd_sync,

     //SPI
     output        spi_csn_clk,
     output        spi_csn_dac,
     output        spi_csn_adc,
     output        spi_clk,
     inout         spi_sdio,
     output        spi_dir,

     //PCIE
     input [3:0]   PCIE_RX_N,
     input [3:0]   PCIE_RX_P,
     output [3:0]  PCIE_TX_N,
     output [3:0]  PCIE_TX_P,
     input         PCIE_PERST_B_LS,
     input         PCIE_REFCLK_N,
     input         PCIE_REFCLK_P,

     output [3:0]  GPIO_LED,
     //FAN
     output        fan_pwm //set to 1
     );
   
   wire [63:0]     gpio_i;
   wire [63:0]     gpio_o;
   wire [63:0]     gpio_t;
   wire [ 7:0]     spi_csn;
   wire            spi_mosi;
   wire            spi_miso;
   wire            trig;
   wire            rx_ref_clk;
   wire            rx_sysref;
   wire            rx_sync;
   wire            tx_ref_clk;
   wire            tx_sysref;
   wire            tx_sync;

   //TO XILLYBUS
   wire            adc_clk;
   wire [127:0]    adc_data;
   wire            adc_wr_en;
   wire 	   adc_rst;
   
   wire            dac_clk;    
   (* mark_debug = "true" *)wire           dac_rd_en;
   wire            dac_rst;
   (* mark_debug = "true" *)wire [127:0]    dac_data;
   
   //XILLYBUS
   wire            bus_clk;
   wire            quiesce;
   
   // Memory array
   //reg [7:0]     demoarray[0:4095];
   
   // Wires related to /dev/xillybus_mem_8
   wire            user_r_mem_8_rden;
   wire            user_r_mem_8_empty;
   reg [7:0]       user_r_mem_8_data;
   wire            user_r_mem_8_eof;
   wire            user_r_mem_8_open;
   wire            user_w_mem_8_wren;
   wire            user_w_mem_8_full;
   wire [7:0]      user_w_mem_8_data;
   wire            user_w_mem_8_open;
   wire [15:0]     user_mem_8_addr;

   // Wires related to /dev/xillybus_read_128
   (* mark_debug = "true" *) wire            user_r_read_128_rden;
   (* mark_debug = "true" *)wire            user_r_read_128_empty;
   wire [127:0]    user_r_read_128_data;
   wire            user_r_read_128_eof;
   (* mark_debug = "true" *)wire            user_r_read_128_open;

   // Wires related to /dev/xillybus_write_128
   (* mark_debug = "true" *)wire           user_w_write_128_wren;
   (* mark_debug = "true" *)wire           user_w_write_128_full;
   (* mark_debug = "true" *)wire [127:0]    user_w_write_128_data;
   (* mark_debug = "true" *)wire           user_w_write_128_open;
   (* mark_debug = "true" *)wire       tx_empty;
   
   // Wires from the block diagram
   (* mark_debug = "true" *) wire [63:0] 	   dac_data_0;
   wire [63:0] 	   dac_data_1;
   (* mark_debug = "true" *) wire 	   dac_valid_0;
   (* mark_debug = "true" *) wire 	   dac_valid_1;
   (* mark_debug = "true" *) wire 	   dac_sync;
   (* mark_debug = "true" *) wire 	   dac_enable_0;

   
   // spi
   assign spi_csn_adc = spi_csn[2];
   assign spi_csn_dac = spi_csn[1];
   assign spi_csn_clk = spi_csn[0];

   // default logic

   assign fan_pwm = 1'b1;
   
   //register space
   (* mark_debug = "true" *) reg             reg_start = 1'b0;
   reg             reg_intr4 = 1'b0;
   reg             reg_intr5 = 1'b0;
   reg [31:0]      reg_num_clk_cycles = 32'd500;
   

   // ADC/DAC buffers
   IBUFDS_GTE3 i_ibufds_rx_ref_clk 
     (.CEB (1'd0),
      .I (rx_ref_clk_p),
      .IB (rx_ref_clk_n),
      .O (rx_ref_clk),
      .ODIV2 ());

   IBUFDS i_ibufds_rx_sysref 
     (.I (rx_sysref_p),
      .IB (rx_sysref_n),
      .O (rx_sysref));

   OBUFDS i_obufds_rx_sync 
     (.I (rx_sync),
      .O (rx_sync_p),
      .OB (rx_sync_n));

   IBUFDS_GTE3 i_ibufds_tx_ref_clk 
     (.CEB (1'd0),
      .I (tx_ref_clk_p),
      .IB (tx_ref_clk_n),
      .O (tx_ref_clk),
      .ODIV2 ());

   IBUFDS i_ibufds_tx_sysref 
     (.I (tx_sysref_p),
      .IB (tx_sysref_n),
      .O (tx_sysref));

   IBUFDS i_ibufds_tx_sync 
     (.I (tx_sync_p),
      .IB (tx_sync_n),
      .O (tx_sync));

   daq2_spi i_spi 
     (
      .spi_csn  (spi_csn[2:0]),
      .spi_clk  (spi_clk),
      .spi_mosi (spi_mosi),
      .spi_miso (spi_miso),
      .spi_sdio (spi_sdio),
      .spi_dir  (spi_dir));

   IBUFDS i_ibufds_trig 
     (
      .I  (trig_p),
      .IB (trig_n),
      .O  (trig));
   
   assign gpio_i[43] = trig;

   ad_iobuf #(.DATA_WIDTH(9)) i_iobuf
     (
      .dio_t ({gpio_t[42:40], gpio_t[38], gpio_t[36:32]}),
      .dio_i ({gpio_o[42:40], gpio_o[38], gpio_o[36:32]}),
      .dio_o ({gpio_i[42:40], gpio_i[38], gpio_i[36:32]}),
      .dio_p ({ adc_pd,           // 42
                dac_txen,         // 41
                dac_reset,        // 40
                clkd_sync,        // 38
                adc_fdb,          // 36
                adc_fda,          // 35
                dac_irq,          // 34
                clkd_status}));   // 32

   ad_iobuf #(.DATA_WIDTH(13)) i_iobuf_bd 
     (
      .dio_t (gpio_t[16:4]),
      .dio_i (gpio_o[16:4]),
      .dio_o (gpio_i[16:4]),
      .dio_p (gpio_bd));
   
   system_wrapper u_system_wrapper 
     (//system
      // Outputs
      .c0_ddr4_act_n    (ddr4_act_n),
      .c0_ddr4_adr      (ddr4_addr[16:0]),
      .c0_ddr4_ba       (ddr4_ba[1:0]),
      .c0_ddr4_bg       (ddr4_bg),
      .c0_ddr4_ck_c     (ddr4_ck_n),
      .c0_ddr4_ck_t     (ddr4_ck_p),
      .c0_ddr4_cke      (ddr4_cke),
      .c0_ddr4_cs_n     (ddr4_cs_n),
      .c0_ddr4_odt      (ddr4_odt),
      .c0_ddr4_reset_n  (ddr4_reset_n),
      .gpio0_o          (gpio_o[31:0]),
      .gpio0_t          (gpio_t[31:0]),
      .gpio1_o          (gpio_o[63:32]),
      .gpio1_t          (gpio_t[63:32]),
      .spi_clk_o        (spi_clk),
      .spi_csn_o        (spi_csn[7:0]),
      .spi_sdo_o        (spi_mosi),
      .uart_sout        (uart_sout),
      // Inouts
      .c0_ddr4_dm_n     (ddr4_dm_n[7:0]),
      .c0_ddr4_dq       (ddr4_dq[63:0]),
      .c0_ddr4_dqs_c    (ddr4_dqs_n[7:0]),
      .c0_ddr4_dqs_t    (ddr4_dqs_p[7:0]),
      // Inputs
      .gpio0_i          (gpio_i[31:0]),
      .gpio1_i          (gpio_i[63:32]),
      .mb_intr_4        (reg_intr4),
      .mb_intr_5        (reg_intr5),
      .spi_clk_i        (spi_clk),
      .spi_csn_i        (spi_csn[7:0]),
      .spi_sdi_i        (spi_miso),
      .spi_sdo_i        (spi_mosi),
      .sys_clk_clk_n    (sys_clk_n),
      .sys_clk_clk_p    (sys_clk_p),
      .sys_rst          (sys_rst),
      .uart_sin         (uart_sin),
      
      // Outputs
      .rx_sync_0        (rx_sync),
      .tx_data_0_n      (tx_data_n[0]),
      .tx_data_0_p      (tx_data_p[0]),
      .tx_data_1_n      (tx_data_n[1]),
      .tx_data_1_p      (tx_data_p[1]),
      .tx_data_2_n      (tx_data_n[2]),
      .tx_data_2_p      (tx_data_p[2]),
      .tx_data_3_n      (tx_data_n[3]),
      .tx_data_3_p      (tx_data_p[3]),
      // Inputs
      .rx_data_0_n      (rx_data_n[0]),
      .rx_data_0_p      (rx_data_p[0]),
      .rx_data_1_n      (rx_data_n[1]),
      .rx_data_1_p      (rx_data_p[1]),
      .rx_data_2_n      (rx_data_n[2]),
      .rx_data_2_p      (rx_data_p[2]),
      .rx_data_3_n      (rx_data_n[3]),
      .rx_data_3_p      (rx_data_p[3]),
      .rx_ref_clk_0     (rx_ref_clk),
      .rx_sysref_0      (rx_sysref),
      .tx_ref_clk_0     (tx_ref_clk),
      .tx_sync_0        (tx_sync),
      .tx_sysref_0      (tx_sysref),
      
      // Outputs
      .adc_clk                          (adc_clk),
      .adc_data                         (adc_data[127:0]),
      .adc_wr_en                        (adc_wr_en),
      .dac_clk                          (dac_clk),
      .dac_rd_en                        (dac_rd_en),
      .dac_rst                          (dac_rst),
      // Inputs
      .dac_data                         (dac_data[127:0]),
      /*AUTOINST*/
      // Outputs
      .adc_rst                          (adc_rst),
      .dac_data_0			(dac_data_0[63:0]),
      .dac_data_1			(dac_data_1[63:0]),
      .dac_valid_0			(dac_valid_0),
      .dac_valid_1			(dac_valid_1),
      .dac_sync                         (dac_sync),
      .dac_enable_0                     (dac_enable_0),
      // Inputs
      .dac_dunf				(tx_empty));

  
   
   xillybus  xillybus_ins 
     (
      // Ports related to /dev/xillybus_mem_8
      // FPGA to CPU signals:
      .user_r_mem_8_rden(user_r_mem_8_rden),
      .user_r_mem_8_empty(user_r_mem_8_empty),
      .user_r_mem_8_data(user_r_mem_8_data),
      .user_r_mem_8_eof(user_r_mem_8_eof),
      .user_r_mem_8_open(user_r_mem_8_open),

      // CPU to FPGA signals:
      .user_w_mem_8_wren(user_w_mem_8_wren),
      .user_w_mem_8_full(user_w_mem_8_full),
      .user_w_mem_8_data(user_w_mem_8_data),
      .user_w_mem_8_open(user_w_mem_8_open),

      // Address signals:
      .user_mem_8_addr(user_mem_8_addr),
      .user_mem_8_addr_update(user_mem_8_addr_update),
      
      // Ports related to /dev/xillybus_read_128
      // FPGA to CPU signals:
      .user_r_read_128_rden(user_r_read_128_rden),
      .user_r_read_128_empty(user_r_read_128_empty),
      .user_r_read_128_data(user_r_read_128_data),
      .user_r_read_128_eof(user_r_read_128_eof),
      .user_r_read_128_open(user_r_read_128_open),

      // Ports related to /dev/xillybus_write_128
      // CPU to FPGA signals:
      .user_w_write_128_wren(user_w_write_128_wren),
      .user_w_write_128_full(user_w_write_128_full),
      .user_w_write_128_data(user_w_write_128_data),
      .user_w_write_128_open(user_w_write_128_open),

      // General signals
      .PCIE_PERST_B_LS(PCIE_PERST_B_LS),
      .PCIE_REFCLK_N(PCIE_REFCLK_N),
      .PCIE_REFCLK_P(PCIE_REFCLK_P),
      .PCIE_RX_N(PCIE_RX_N),
      .PCIE_RX_P(PCIE_RX_P),
      .GPIO_LED(GPIO_LED),
      .PCIE_TX_N(PCIE_TX_N),
      .PCIE_TX_P(PCIE_TX_P),
      .bus_clk(bus_clk),
      .quiesce(quiesce)
      );
  
   
   //
   reg [2:0]       reg_start_adc = 3'b000;
   wire            reg_start_r;
   reg [31:0]      rx_cnt = 32'd0;
   wire            wr_adc_data;
   reg [2:0]       user_r_read_128_open_adc = 3'b000;
   reg [2:0]       wr_adc_data_bus = 3'b000;
   wire            wr_adc_data_r;
   reg             wr_adc_data_d = 1'b0;
   wire            user_r_read_128_open_adc_r;

   
   (* mark_debug = "true" *)wire       tx_prog_full;
   (* mark_debug = "true" *)wire [10:0] tx_wr_data_count;
   
   
   always@(posedge bus_clk)
     begin
        if(user_w_mem_8_wren)
          case(user_mem_8_addr)
            8'd0 : begin
               reg_start                 <= user_w_mem_8_data[0];
               reg_intr4                 <= user_w_mem_8_data[1];
               reg_intr5                 <= user_w_mem_8_data[2];
               
            end
            8'd1 : reg_num_clk_cycles[07:00] <= user_w_mem_8_data;
            8'd2 : reg_num_clk_cycles[15:08] <= user_w_mem_8_data;
            8'd3 : reg_num_clk_cycles[23:16] <= user_w_mem_8_data;
            8'd4 : reg_num_clk_cycles[31:24] <= user_w_mem_8_data;
            
          endcase // case (user_w_mem_8_addr)
        else begin //toggle registers
           reg_start <= wr_adc_data_bus_r ? 1'b0 : reg_start; //special case
           
           reg_intr4                 <= &intr_cntr ? 1'b0 : reg_intr4;
           reg_intr5                 <= &intr_cntr ? 1'b0 : reg_intr5;  
           
        end
        if(user_r_mem_8_rden)
          case(user_mem_8_addr)
            8'd0 : user_r_mem_8_data <= reg_start;
            8'd1 : user_r_mem_8_data <= reg_num_clk_cycles[7:0];
            8'd2 : user_r_mem_8_data <= reg_num_clk_cycles[15:8];
            8'd3 : user_r_mem_8_data <= reg_num_clk_cycles[23:16];
            8'd4 : user_r_mem_8_data <= reg_num_clk_cycles[31:24];
          endcase // case (user_w_mem_8_addr)
     end

   reg [2:0] intr_cntr = 3'd0;

   always@(posedge bus_clk)begin
      if(reg_intr4 | reg_intr5)
        intr_cntr <= intr_cntr + 1'b1;
   end
   
   assign  user_r_mem_8_empty = 0;
   assign  user_r_mem_8_eof = 0;
   assign  user_w_mem_8_full = 0;
   
   //read gating
   //assumes adc_clk will be equal to or slower than bus_clk at all times
   always@(posedge adc_clk)begin
      reg_start_adc <= {reg_start_adc[1:0],reg_start};
      user_r_read_128_open_adc <= {user_r_read_128_open_adc[1:0], user_r_read_128_open}; //CDC 
   end
   
   assign user_r_read_128_open_adc_r = user_r_read_128_open_adc[1] & !user_r_read_128_open_adc[2];
   
   always@(posedge adc_clk) begin
      if(rx_cnt == reg_num_clk_cycles[31:0])
        rx_cnt <= 32'd0;
      else if(wr_adc_data && adc_wr_en)
        rx_cnt <= rx_cnt + 1'b1;
      else if(reg_start_adc[1] && user_r_read_128_open_adc[1])
        rx_cnt <= 1'b1;
      else
        rx_cnt <= rx_cnt;       
   end


   assign wr_adc_data = |rx_cnt;
   
   always@(posedge adc_clk)
     wr_adc_data_d <= wr_adc_data;
   
   always@(posedge bus_clk)begin
      wr_adc_data_bus <= {wr_adc_data_bus[1:0],wr_adc_data_d}; 
   end

   //use to reset reg_start register
   //clock domain crossing safety nets implemented
   //delays means there is a minimum number of data that must be transferred that is dependent on clock speed
   assign wr_adc_data_bus_r = wr_adc_data_bus[1] && !wr_adc_data_bus[2];

   //TX reg 
   (* mark_debug = "true" *)reg [2:0] tx_prog_full_dac = 3'b000;
   (* mark_debug = "true" *)reg       dac_en = 1'b0;
   (* mark_debug = "true" *)reg       tx_empty_d = 1'b0;
   (* mark_debug = "true" *)wire      tx_empty_r;       
   
   //FROM ADC
   //Host read must be setup first
   //Dependent on fifo being empty until trigger is sent
   //wr_adc_data goes high only when trigger is sent from host

   wire [63:0] adc_data_0;
   wire [63:0] adc_data_1;
   wire        bypass;
   wire [127:0] adc_data_bypass;
   wire 	pkt_detect;
   wire [63:0] 	aligned_re;
   wire [63:0] 	aligned_im;
   wire 	aligned_valid;
   wire 	rx_write_en;
   
   assign adc_data_0 = {adc_data[111:96],adc_data[79:64],adc_data[47:32],adc_data[15:0]};
   assign adc_data_1 = {adc_data[127:112],adc_data[95:80],adc_data[63:48],adc_data[31:16]}; 
     
   timesync_wrapper u_timesync_wrapper
     (.clk           (adc_clk),
      .reset         (adc_rst),
      .clk_enable    (1'b1),
      .sample_in_re  (adc_data_0[63:0]),
      .sample_in_im  (adc_data_1[63:0]),
      .valid_in      (adc_wr_en),
      .pkt_detect    (pkt_detect),
      .aligned_re    (aligned_re[63:0]),
      .aligned_im    (aligned_im[63:0]),
      .aligned_valid (aligned_valid)
      );

   assign bypass = 1'b0;
   
   assign adc_data_bypass = bypass ?  adc_data[127:0] : {aligned_re,aligned_im};
   assign rx_write_en = bypass ? adc_wr_en : aligned_valid;
   
   fifo_generator_0 u_rx_fifo 
     (
      .rst(!user_r_read_128_open),     // input wire rst
      .wr_clk(adc_clk),                   // input wire wr_clk
      .din(adc_data_bypass[127:0]),              // input wire [127 : 0] din
      .wr_en(rx_write_en & wr_adc_data),    // input wire wr_en

      .rd_clk(bus_clk),                   // input wire rd_clk
      .dout(user_r_read_128_data[127:0]), // output wire [127 : 0] dout
      .rd_en(user_r_read_128_rden),       // input wire rd_en
      
      .full(),                            // output wire full
      .empty(user_r_read_128_empty),      // output wire empty
      .valid(),                           // output wire valid
      .prog_full()                       // output wire prog_full
      //.wr_rst_busy(),                     // output wire wr_rst_busy
      //.rd_rst_busy()                      // output wire rd_rst_busy
      );

   reg user_w_write_128_open_d = 1'b0;
   wire user_w_write_128_open_r;
   
   //TO DAC
   fifo_generator_0 u_tx_fifo 
     (
      .rst(!user_w_write_128_open),       // input wire rst
      .wr_clk(bus_clk),                   // input wire wr_clk
      .din(user_w_write_128_data[127:0]), // input wire [127 : 0] din
      .wr_en(user_w_write_128_wren),      // input wire wr_en

      .rd_clk(dac_clk),                   // input wire rd_clk
      .dout(dac_data[127:0]),             // output wire [127 : 0] dout
      .rd_en(dac_rd_en),// && dac_en),        // input wire rd_en

      .wr_data_count(tx_wr_data_count[10:0]),
      .full(user_w_write_128_full),       // output wire full
      .empty(tx_empty),                   // output wire empty
      .valid(),                           // output wire valid
      .prog_full(tx_prog_full)           // output wire prog_full, right now it is at 499
      //.wr_rst_busy(),                     // output wire wr_rst_busy
      //.rd_rst_busy()                      // output wire rd_rst_busy
      );


   always@(posedge bus_clk)
     user_w_write_128_open_d <= user_w_write_128_open;
   
   assign user_r_read_128_eof = 0;
   
   assign user_w_write_128_open_r = user_w_write_128_open & !user_w_write_128_open_d;
   
   always@(posedge dac_clk)begin
      tx_prog_full_dac <= {tx_prog_full_dac[1:0], tx_prog_full};
      tx_empty_d <= tx_empty;
      
      if(tx_prog_full_dac_r)
        dac_en <= 1'b1;
      else if(tx_empty_r)
        dac_en <= 1'b0;         
   end

   assign tx_empty_r = tx_empty & !tx_empty_d; 
   assign tx_prog_full_dac_r = tx_prog_full_dac[1] & !tx_prog_full_dac[2];
   
   
   /* reg_space u_reg_space
    (
    // Outputs
    .S_AXI_AWREADY                    (m_awready),
    .S_AXI_WREADY                     (m_wready),
    .S_AXI_BRESP                      (m_bresp[1:0]),
    .S_AXI_BVALID                     (m_bvalid),
    .S_AXI_ARREADY                    (m_arready),
    .S_AXI_RDATA                      (m_rdata[31:0]),
    .S_AXI_RRESP                      (m_rresp[1:0]),
    .S_AXI_RVALID                     (m_rvalid),
    // Inputs
    .S_AXI_ACLK                       (axi_aclk),
    .S_AXI_ARESETN                    (axi_aresetn),
    .S_AXI_AWADDR                     (m_awaddr[31:0]),
    .S_AXI_AWVALID                    (m_awvalid),
    .S_AXI_WDATA                      (m_wdata[31:0]),
    .S_AXI_WSTRB                      (m_wstrb[3:0]),
    .S_AXI_WVALID                     (m_wvalid),
    .S_AXI_BREADY                     (m_bready),
    .S_AXI_ARADDR                     (m_araddr[31:0]),
    .S_AXI_ARVALID                    (m_arvalid),
    .S_AXI_RREADY                     (m_arready));*/
   

endmodule
