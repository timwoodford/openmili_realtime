----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/28/2019 01:48:12 PM
-- Design Name: 
-- Module Name: common_types - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package common_types is
  type vector_of_std_logic_vector14 is array (natural range <>) of std_logic_vector(13 downto 0);
  type vector_of_std_logic_vector16 is array (natural range <>) of std_logic_vector(15 downto 0);
  type vector_of_std_logic_vector17 is array (natural range <>) of std_logic_vector(16 downto 0);
  type vector_of_std_logic_vector25 is array (natural range <>) of std_logic_vector(24 downto 0);
end package;