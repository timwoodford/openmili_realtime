#tcl script is based off of fmcdaq2 example project
#this tcl script should be sourced after project is created and an ip block is created
#project and ipblock creation is not automated
#make sure that the example design has been built once correctly
#we will be using the prebuilt ips from the example run to simplify implementation

#set environment related stuff

set ad_hdl_dir C:/hdl-master
set ad_phdl_dir $ad_hdl_dir

#source custom commands from ADI (they are very useful)
source $ad_hdl_dir/projects/scripts/adi_project.tcl
source $ad_hdl_dir/projects/scripts/adi_board.tcl
source $ad_hdl_dir/projects/scripts/adi_xilinx_msg.tcl
source $ad_hdl_dir/library/jesd204/scripts/jesd204.tcl

#update ip catalog with ADI IPs
set lib_dirs $ad_hdl_dir/library
set_property ip_repo_paths $lib_dirs [current_fileset]
update_ip_catalog

set sys_zynq 0
# *************************************************************************************** #

# ports : reset
create_bd_port -dir I -type rst sys_rst

set_property -dict [list CONFIG.POLARITY {ACTIVE_HIGH}] [get_bd_ports sys_rst]

# ports : uart
create_bd_port -dir I uart_sin
create_bd_port -dir O uart_sout

# ports : interrupt
create_bd_port -dir I -type intr mb_intr_2
create_bd_port -dir I -type intr mb_intr_3
create_bd_port -dir I -type intr mb_intr_4
create_bd_port -dir I -type intr mb_intr_5
create_bd_port -dir I -type intr mb_intr_6
create_bd_port -dir I -type intr mb_intr_7
create_bd_port -dir I -type intr mb_intr_8

# ports : clock
create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 sys_clk
create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 phy_clk

set_property -dict [list CONFIG.FREQ_HZ {300000000}] [get_bd_intf_ports sys_clk]
set_property -dict [list CONFIG.FREQ_HZ {625000000}] [get_bd_intf_ports phy_clk]

# ports : spi
create_bd_port -dir O -from 7 -to 0 spi_csn_o
create_bd_port -dir I -from 7 -to 0 spi_csn_i
create_bd_port -dir I spi_clk_i
create_bd_port -dir O spi_clk_o
create_bd_port -dir I spi_sdo_i
create_bd_port -dir O spi_sdo_o
create_bd_port -dir I spi_sdi_i

# ports : DDR4
create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddr4_rtl:1.0 c0_ddr4

# ports : gpios
create_bd_port -dir I -from 31 -to 0 gpio0_i
create_bd_port -dir O -from 31 -to 0 gpio0_o
create_bd_port -dir O -from 31 -to 0 gpio0_t
create_bd_port -dir I -from 31 -to 0 gpio1_i
create_bd_port -dir O -from 31 -to 0 gpio1_o
create_bd_port -dir O -from 31 -to 0 gpio1_t


# *************************************************************************************** #

# instance : reset
ad_ip_instance proc_sys_reset sys_rstgen

# instance : uart
ad_ip_instance axi_uartlite axi_uart
ad_ip_parameter axi_uart CONFIG.C_BAUDRATE 115200

# instance : spi
ad_ip_instance axi_quad_spi axi_spi
ad_ip_parameter axi_spi CONFIG.C_USE_STARTUP 0
ad_ip_parameter axi_spi CONFIG.C_NUM_SS_BITS 8
ad_ip_parameter axi_spi CONFIG.C_SCK_RATIO 8

# instance: interrupt
ad_ip_instance axi_intc axi_intc
ad_ip_parameter axi_intc CONFIG.C_HAS_FAST 0

ad_ip_instance xlconcat sys_concat_intc
ad_ip_parameter sys_concat_intc CONFIG.NUM_PORTS 9

# instance: microblaze - processor
ad_ip_instance microblaze sys_mb
ad_ip_parameter sys_mb CONFIG.G_TEMPLATE_LIST 4
#ad_ip_parameter sys_mb CONFIG.C_DCACHE_FORCE_TAG_LUTRAM 1

# instance: microblaze - local memory & bus
ad_ip_instance lmb_v10 sys_dlmb
ad_ip_instance lmb_v10 sys_ilmb

ad_ip_instance lmb_bram_if_cntlr sys_dlmb_cntlr
ad_ip_parameter sys_dlmb_cntlr CONFIG.C_ECC 0

ad_ip_instance lmb_bram_if_cntlr sys_ilmb_cntlr
ad_ip_parameter sys_ilmb_cntlr CONFIG.C_ECC 0

ad_ip_instance blk_mem_gen sys_lmb_bram
ad_ip_parameter sys_lmb_bram CONFIG.Memory_Type True_Dual_Port_RAM
ad_ip_parameter sys_lmb_bram CONFIG.use_bram_block BRAM_Controller

# instance: microblaze- mdm
ad_ip_instance mdm sys_mb_debug
ad_ip_parameter sys_mb_debug CONFIG.C_USE_UART 1

# instance: ddr4
ad_ip_instance ip:ddr4 axi_ddr_cntrl
ad_ip_parameter axi_ddr_cntrl CONFIG.C0_CLOCK_BOARD_INTERFACE default_sysclk_300
ad_ip_parameter axi_ddr_cntrl CONFIG.C0_DDR4_BOARD_INTERFACE ddr4_sdram
ad_ip_parameter axi_ddr_cntrl CONFIG.RESET_BOARD_INTERFACE reset
ad_ip_parameter axi_ddr_cntrl CONFIG.ADDN_UI_CLKOUT2_FREQ_HZ 200

ad_ip_instance proc_sys_reset axi_ddr_cntrl_rstgen

# instance : gpio
ad_connect  gpio0_i axi_gpio/gpio_io_i
ad_connect  gpio0_o axi_gpio/gpio_io_o
ad_connect  gpio0_t axi_gpio/gpio_io_t
ad_connect  gpio1_i axi_gpio/gpio2_io_i
ad_connect  gpio1_o axi_gpio/gpio2_io_o
ad_connect  gpio1_t axi_gpio/gpio2_io_t

# *************************************************************************************** #

# connections : reset
ad_connect  sys_cpu_reset sys_rstgen/peripheral_reset
ad_connect  sys_cpu_resetn sys_rstgen/peripheral_aresetn

# connections : uart
ad_connect  uart_sin axi_uart/rx
ad_connect  uart_sout axi_uart/tx

# connections : ddr4
ad_connect  sys_rst axi_ddr_cntrl/sys_rst
ad_connect  sys_clk axi_ddr_cntrl/C0_SYS_CLK
ad_connect  c0_ddr4 axi_ddr_cntrl/C0_DDR4
ad_connect  axi_ddr_cntrl/c0_ddr4_ui_clk_sync_rst sys_rstgen/ext_reset_in
ad_connect  axi_ddr_cntrl/c0_ddr4_ui_clk_sync_rst axi_ddr_cntrl_rstgen/ext_reset_in
ad_connect  sys_mem_clk axi_ddr_cntrl/c0_ddr4_ui_clk
ad_connect  sys_mem_clk axi_ddr_cntrl_rstgen/slowest_sync_clk
ad_connect  sys_cpu_clk axi_ddr_cntrl/addn_ui_clkout1
ad_connect  sys_cpu_clk sys_rstgen/slowest_sync_clk
ad_connect  sys_mem_resetn axi_ddr_cntrl_rstgen/peripheral_aresetn
ad_connect  sys_mem_resetn axi_ddr_cntrl/c0_ddr4_aresetn
ad_connect  sys_200m_clk axi_ddr_cntrl/addn_ui_clkout2

# connections : spi
ad_connect  spi_csn_i axi_spi/ss_i
ad_connect  spi_csn_o axi_spi/ss_o
ad_connect  spi_clk_i axi_spi/sck_i
ad_connect  spi_clk_o axi_spi/sck_o
ad_connect  spi_sdo_i axi_spi/io0_i
ad_connect  spi_sdo_o axi_spi/io0_o
ad_connect  spi_sdi_i axi_spi/io1_i
ad_connect  sys_cpu_clk axi_spi/ext_spi_clk

# connections : microblaze
ad_connect  sys_cpu_clk sys_mb/Clk
ad_connect  sys_cpu_clk sys_dlmb/LMB_Clk
ad_connect  sys_cpu_clk sys_ilmb/LMB_Clk
ad_connect  sys_cpu_clk sys_dlmb_cntlr/LMB_Clk
ad_connect  sys_cpu_clk sys_ilmb_cntlr/LMB_Clk
ad_connect  sys_rstgen/mb_reset sys_mb/Reset
ad_connect  sys_rstgen/bus_struct_reset sys_dlmb/SYS_Rst
ad_connect  sys_rstgen/bus_struct_reset sys_ilmb/SYS_Rst
ad_connect  sys_rstgen/bus_struct_reset sys_dlmb_cntlr/LMB_Rst
ad_connect  sys_rstgen/bus_struct_reset sys_ilmb_cntlr/LMB_Rst
ad_connect  sys_mb/DLMB sys_dlmb/LMB_M
ad_connect  sys_mb/ILMB sys_ilmb/LMB_M
ad_connect  sys_dlmb/LMB_Sl_0 sys_dlmb_cntlr/SLMB
ad_connect  sys_ilmb/LMB_Sl_0 sys_ilmb_cntlr/SLMB
ad_connect  sys_dlmb_cntlr/BRAM_PORT sys_lmb_bram/BRAM_PORTA
ad_connect  sys_ilmb_cntlr/BRAM_PORT sys_lmb_bram/BRAM_PORTB
ad_connect  sys_mb_debug/Debug_SYS_Rst sys_rstgen/mb_debug_sys_rst
ad_connect  sys_mb_debug/MBDEBUG_0 sys_mb/DEBUG
ad_connect  axi_intc/interrupt sys_mb/INTERRUPT
ad_connect  axi_intc/intr sys_concat_intc/dout

# connections : interrupt 
ad_connect  sys_concat_intc/In0 axi_spi/ip2intc_irpt
ad_connect  sys_concat_intc/In1 axi_uart/interrupt
ad_connect  sys_concat_intc/In11 axi_gpio/ip2intc_irpt
ad_connect  sys_concat_intc/In3 mb_intr_3
ad_connect  sys_concat_intc/In4 mb_intr_4
ad_connect  sys_concat_intc/In5 mb_intr_5
ad_connect  sys_concat_intc/In6 mb_intr_6
ad_connect  sys_concat_intc/In7 mb_intr_7
ad_connect  sys_concat_intc/In8 mb_intr_8

# interconnect - processor
ad_cpu_interconnect 0x41400000 sys_mb_debug
ad_cpu_interconnect 0x44A70000 axi_spi
ad_cpu_interconnect 0x41200000 axi_intc
ad_cpu_interconnect 0x40600000 axi_uart
ad_cpu_interconnect 0x40000000 axi_gpio
#ad_cpu_interconnect 0x40E00000 pcie3_dma_subsystem

# interconnect - memory
ad_mem_hp0_interconnect sys_mem_clk axi_ddr_cntrl/C0_DDR4_S_AXI
ad_mem_hp0_interconnect sys_cpu_clk sys_mb/M_AXI_DC
ad_mem_hp0_interconnect sys_cpu_clk sys_mb/M_AXI_IC

ad_disconnect  sys_mem_clk axi_mem_interconnect/ACLK
ad_disconnect  sys_mem_resetn axi_mem_interconnect/ARESETN

ad_connect  sys_cpu_clk axi_mem_interconnect/ACLK
ad_connect  sys_cpu_resetn axi_mem_interconnect/ARESETN

create_bd_addr_seg -range 0x20000 -offset 0x0 [get_bd_addr_spaces sys_mb/Data] \
  [get_bd_addr_segs sys_dlmb_cntlr/SLMB/Mem] SEG_dlmb_cntlr
create_bd_addr_seg -range 0x20000 -offset 0x0 [get_bd_addr_spaces sys_mb/Instruction] \
    [get_bd_addr_segs sys_ilmb_cntlr/SLMB/Mem] SEG_ilmb_cntlr

#pcie subsystem

#ad_ip_instance xdma pcie3_dma_subsystem


# *************************************************************************************** #


# FIFOs
set adc_fifo_name axi_ad9680_fifo
set adc_fifo_address_width 16
set adc_data_width 128
set adc_dma_data_width 64

set dac_fifo_name axi_ad9144_fifo
set dac_fifo_address_width 10
set dac_data_width 128
set dac_dma_data_width 128

ad_ip_instance util_adcfifo $adc_fifo_name
ad_ip_parameter $adc_fifo_name CONFIG.ADC_DATA_WIDTH $adc_data_width
ad_ip_parameter $adc_fifo_name CONFIG.DMA_DATA_WIDTH $adc_dma_data_width
ad_ip_parameter $adc_fifo_name CONFIG.DMA_READY_ENABLE 1
ad_ip_parameter $adc_fifo_name CONFIG.DMA_ADDRESS_WIDTH $adc_fifo_address_width

if {$dac_data_width != $dac_dma_data_width} {
  return -code error [format "ERROR: util_dacfifo dac/dma widths must be the same!"]
}

ad_ip_instance util_dacfifo $dac_fifo_name
ad_ip_parameter $dac_fifo_name CONFIG.DATA_WIDTH $dac_data_width
ad_ip_parameter $dac_fifo_name CONFIG.ADDRESS_WIDTH $dac_fifo_address_width

# instance : DAC peripherals
ad_ip_instance axi_adxcvr axi_ad9144_xcvr
ad_ip_parameter axi_ad9144_xcvr CONFIG.NUM_OF_LANES 4
ad_ip_parameter axi_ad9144_xcvr CONFIG.QPLL_ENABLE 1
ad_ip_parameter axi_ad9144_xcvr CONFIG.TX_OR_RX_N 1

adi_axi_jesd204_tx_create axi_ad9144_jesd 4

ad_ip_instance axi_ad9144 axi_ad9144_core
ad_ip_parameter axi_ad9144_core CONFIG.QUAD_OR_DUAL_N 0

ad_ip_instance util_upack axi_ad9144_upack
ad_ip_parameter axi_ad9144_upack CONFIG.CHANNEL_DATA_WIDTH 64
ad_ip_parameter axi_ad9144_upack CONFIG.NUM_OF_CHANNELS 2

ad_ip_instance axi_dmac axi_ad9144_dma
ad_ip_parameter axi_ad9144_dma CONFIG.DMA_TYPE_SRC 0
ad_ip_parameter axi_ad9144_dma CONFIG.DMA_TYPE_DEST 1
ad_ip_parameter axi_ad9144_dma CONFIG.ID 1
ad_ip_parameter axi_ad9144_dma CONFIG.AXI_SLICE_SRC 0
ad_ip_parameter axi_ad9144_dma CONFIG.AXI_SLICE_DEST 0
ad_ip_parameter axi_ad9144_dma CONFIG.DMA_LENGTH_WIDTH 24
ad_ip_parameter axi_ad9144_dma CONFIG.DMA_2D_TRANSFER 0
ad_ip_parameter axi_ad9144_dma CONFIG.CYCLIC 0
ad_ip_parameter axi_ad9144_dma CONFIG.DMA_DATA_WIDTH_SRC 128
ad_ip_parameter axi_ad9144_dma CONFIG.DMA_DATA_WIDTH_DEST 128


# instance : ADC peripherals
ad_ip_instance axi_adxcvr axi_ad9680_xcvr
ad_ip_parameter axi_ad9680_xcvr CONFIG.NUM_OF_LANES 4
ad_ip_parameter axi_ad9680_xcvr CONFIG.QPLL_ENABLE 0
ad_ip_parameter axi_ad9680_xcvr CONFIG.TX_OR_RX_N 0

adi_axi_jesd204_rx_create axi_ad9680_jesd 4

ad_ip_instance axi_ad9680 axi_ad9680_core

ad_ip_instance util_cpack axi_ad9680_cpack
ad_ip_parameter axi_ad9680_cpack CONFIG.CHANNEL_DATA_WIDTH 64
ad_ip_parameter axi_ad9680_cpack CONFIG.NUM_OF_CHANNELS 2

ad_ip_instance axi_dmac axi_ad9680_dma
ad_ip_parameter axi_ad9680_dma CONFIG.DMA_TYPE_SRC 1
ad_ip_parameter axi_ad9680_dma CONFIG.DMA_TYPE_DEST 0
ad_ip_parameter axi_ad9680_dma CONFIG.ID 0
ad_ip_parameter axi_ad9680_dma CONFIG.AXI_SLICE_SRC 0
ad_ip_parameter axi_ad9680_dma CONFIG.AXI_SLICE_DEST 0
ad_ip_parameter axi_ad9680_dma CONFIG.SYNC_TRANSFER_START 0
ad_ip_parameter axi_ad9680_dma CONFIG.DMA_LENGTH_WIDTH 24
ad_ip_parameter axi_ad9680_dma CONFIG.DMA_2D_TRANSFER 0
ad_ip_parameter axi_ad9680_dma CONFIG.CYCLIC 0
ad_ip_parameter axi_ad9680_dma CONFIG.DMA_DATA_WIDTH_SRC 64
ad_ip_parameter axi_ad9680_dma CONFIG.DMA_DATA_WIDTH_DEST 64

#shared tx/rx core

ad_ip_instance util_adxcvr util_daq2_xcvr
ad_ip_parameter util_daq2_xcvr CONFIG.RX_NUM_OF_LANES 4
ad_ip_parameter util_daq2_xcvr CONFIG.TX_NUM_OF_LANES 4

ad_connect  sys_cpu_resetn util_daq2_xcvr/up_rstn
ad_connect  sys_cpu_clk util_daq2_xcvr/up_clk

# reference clocks & resets

create_bd_port -dir I tx_ref_clk_0
create_bd_port -dir I rx_ref_clk_0

ad_xcvrpll  tx_ref_clk_0 util_daq2_xcvr/qpll_ref_clk_*
ad_xcvrpll  rx_ref_clk_0 util_daq2_xcvr/cpll_ref_clk_*
ad_xcvrpll  axi_ad9144_xcvr/up_pll_rst util_daq2_xcvr/up_qpll_rst_*
ad_xcvrpll  axi_ad9680_xcvr/up_pll_rst util_daq2_xcvr/up_cpll_rst_*

# connections : DAC
#make sure to set variables to default
set xcvr_index -1
set xcvr_tx_index 0
set xcvr_rx_index 0
set xcvr_instance NONE
ad_xcvrcon  util_daq2_xcvr axi_ad9144_xcvr axi_ad9144_jesd {0 2 3 1}
ad_connect  util_daq2_xcvr/tx_out_clk_0 axi_ad9144_core/tx_clk
ad_connect  axi_ad9144_jesd/tx_data_tdata axi_ad9144_core/tx_data
ad_connect  util_daq2_xcvr/tx_out_clk_0 axi_ad9144_upack/dac_clk
ad_connect  axi_ad9144_core/dac_enable_0 axi_ad9144_upack/dac_enable_0
ad_connect  axi_ad9144_core/dac_ddata_0 axi_ad9144_upack/dac_data_0
ad_connect  axi_ad9144_core/dac_valid_0 axi_ad9144_upack/dac_valid_0
ad_connect  axi_ad9144_core/dac_enable_1 axi_ad9144_upack/dac_enable_1
ad_connect  axi_ad9144_core/dac_ddata_1 axi_ad9144_upack/dac_data_1
ad_connect  axi_ad9144_core/dac_valid_1 axi_ad9144_upack/dac_valid_1
ad_connect  util_daq2_xcvr/tx_out_clk_0 axi_ad9144_fifo/dac_clk
ad_connect  axi_ad9144_jesd_rstgen/peripheral_reset axi_ad9144_fifo/dac_rst
ad_connect  axi_ad9144_upack/dac_valid axi_ad9144_fifo/dac_valid
ad_connect  axi_ad9144_upack/dac_data axi_ad9144_fifo/dac_data
ad_connect  axi_ad9144_core/dac_dunf axi_ad9144_fifo/dac_dunf
ad_connect  sys_cpu_clk axi_ad9144_fifo/dma_clk
ad_connect  sys_cpu_reset axi_ad9144_fifo/dma_rst
ad_connect  sys_cpu_clk axi_ad9144_dma/m_axis_aclk
ad_connect  sys_cpu_resetn axi_ad9144_dma/m_src_axi_aresetn
ad_connect  axi_ad9144_fifo/dma_xfer_req axi_ad9144_dma/m_axis_xfer_req
ad_connect  axi_ad9144_fifo/dma_ready axi_ad9144_dma/m_axis_ready
ad_connect  axi_ad9144_fifo/dma_data axi_ad9144_dma/m_axis_data
ad_connect  axi_ad9144_fifo/dma_valid axi_ad9144_dma/m_axis_valid
ad_connect  axi_ad9144_fifo/dma_xfer_last axi_ad9144_dma/m_axis_last

# connections : ADC
# xcvrcon creates reset block
ad_xcvrcon  util_daq2_xcvr axi_ad9680_xcvr axi_ad9680_jesd
ad_connect  util_daq2_xcvr/rx_out_clk_0 axi_ad9680_core/rx_clk
ad_connect  axi_ad9680_jesd/rx_sof axi_ad9680_core/rx_sof
ad_connect  axi_ad9680_jesd/rx_data_tdata axi_ad9680_core/rx_data
ad_connect  util_daq2_xcvr/rx_out_clk_0 axi_ad9680_cpack/adc_clk
ad_connect  axi_ad9680_jesd_rstgen/peripheral_reset axi_ad9680_cpack/adc_rst
ad_connect  axi_ad9680_core/adc_enable_0 axi_ad9680_cpack/adc_enable_0
ad_connect  axi_ad9680_core/adc_valid_0 axi_ad9680_cpack/adc_valid_0
ad_connect  axi_ad9680_core/adc_data_0 axi_ad9680_cpack/adc_data_0
ad_connect  axi_ad9680_core/adc_enable_1 axi_ad9680_cpack/adc_enable_1
ad_connect  axi_ad9680_core/adc_valid_1 axi_ad9680_cpack/adc_valid_1
ad_connect  axi_ad9680_core/adc_data_1 axi_ad9680_cpack/adc_data_1
ad_connect  util_daq2_xcvr/rx_out_clk_0 axi_ad9680_fifo/adc_clk
ad_connect  axi_ad9680_jesd_rstgen/peripheral_reset axi_ad9680_fifo/adc_rst
ad_connect  axi_ad9680_cpack/adc_valid axi_ad9680_fifo/adc_wr
ad_connect  axi_ad9680_cpack/adc_data axi_ad9680_fifo/adc_wdata
ad_connect  sys_cpu_clk axi_ad9680_fifo/dma_clk
ad_connect  sys_cpu_clk axi_ad9680_dma/s_axis_aclk
ad_connect  sys_cpu_resetn axi_ad9680_dma/m_dest_axi_aresetn
ad_connect  axi_ad9680_fifo/dma_wr axi_ad9680_dma/s_axis_valid
ad_connect  axi_ad9680_fifo/dma_wdata axi_ad9680_dma/s_axis_data
ad_connect  axi_ad9680_fifo/dma_wready axi_ad9680_dma/s_axis_ready
ad_connect  axi_ad9680_fifo/dma_xfer_req axi_ad9680_dma/s_axis_xfer_req
ad_connect  axi_ad9680_core/adc_dovf axi_ad9680_fifo/adc_wovf

# cpu interconnect

ad_cpu_interconnect 0x44A60000 axi_ad9144_xcvr
ad_cpu_interconnect 0x44A00000 axi_ad9144_core
ad_cpu_interconnect 0x44A90000 axi_ad9144_jesd
ad_cpu_interconnect 0x7c420000 axi_ad9144_dma
ad_cpu_interconnect 0x44A50000 axi_ad9680_xcvr
ad_cpu_interconnect 0x44A10000 axi_ad9680_core
ad_cpu_interconnect 0x44AA0000 axi_ad9680_jesd
ad_cpu_interconnect 0x7c400000 axi_ad9680_dma

# gt uses hp3, and 100MHz clock for both DRP and AXI4

ad_mem_hp3_interconnect sys_cpu_clk sys_ps7/S_AXI_HP3
ad_mem_hp3_interconnect sys_cpu_clk axi_ad9680_xcvr/m_axi

# interconnect (mem/dac)

ad_mem_hp1_interconnect sys_cpu_clk sys_ps7/S_AXI_HP1
ad_mem_hp1_interconnect sys_cpu_clk axi_ad9144_dma/m_src_axi
ad_mem_hp2_interconnect sys_cpu_clk sys_ps7/S_AXI_HP2
ad_mem_hp2_interconnect sys_cpu_clk axi_ad9680_dma/m_dest_axi

# interrupts

ad_cpu_interrupt ps-10 mb-2 axi_ad9144_jesd/irq
ad_cpu_interrupt ps-11 mb-3 axi_ad9680_jesd/irq
ad_cpu_interrupt ps-12 mb-4 axi_ad9144_dma/irq
ad_cpu_interrupt ps-13 mb-5 axi_ad9680_dma/irq

#manually change name of IP to something shorter
ad_connect  axi_ad9144_fifo/bypass GND

ad_ip_parameter axi_ad9144_xcvr CONFIG.XCVR_TYPE 1
ad_ip_parameter axi_ad9680_xcvr CONFIG.XCVR_TYPE 1

ad_ip_parameter util_daq2_xcvr CONFIG.XCVR_TYPE 1
ad_ip_parameter util_daq2_xcvr CONFIG.QPLL_FBDIV 20
ad_ip_parameter util_daq2_xcvr CONFIG.QPLL_REFCLK_DIV 1


#save_bd_design
#validate_bd_design
