-- -------------------------------------------------------------
-- 
-- File Name: build/hdl_prj/timesync/hdl_generation_model_fix/end_det_fn_FixPt.vhd
-- 
-- Generated by MATLAB 9.5 and HDL Coder 3.13
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: end_det_fn_FixPt
-- Source Path: timesync/end_det_fn/end_det_fn_FixPt
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE work.timesync_pkg.ALL;
            use work.common_types.all;

ENTITY end_det_fn_FixPt IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        buffer_re                         :   IN    vector_of_std_logic_vector16(0 TO 143);  -- sfix16_En14 [144]
        buffer_im                         :   IN    vector_of_std_logic_vector16(0 TO 143);  -- sfix16_En14 [144]
        valid                             :   IN    std_logic;
        align_index                       :   IN    std_logic_vector(15 DOWNTO 0);  -- ufix16_En14
        end_detected                      :   OUT   std_logic
        );
END end_det_fn_FixPt;


ARCHITECTURE rtl OF end_det_fn_FixPt IS

  -- Functions
  -- HDLCODER_TO_STDLOGIC 
  FUNCTION hdlcoder_to_stdlogic(arg: boolean) RETURN std_logic IS
  BEGIN
    IF arg THEN
      RETURN '1';
    ELSE
      RETURN '0';
    END IF;
  END FUNCTION;


  -- Signals
  SIGNAL buffer_re_signed                 : vector_of_signed16(0 TO 143);  -- sfix16_En14 [144]
  SIGNAL buffer_im_signed                 : vector_of_signed16(0 TO 143);  -- sfix16_En14 [144]
  SIGNAL align_index_unsigned             : unsigned(15 DOWNTO 0);  -- ufix16_En14
  SIGNAL last_buffer_reg1_re              : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL last_buffer_reg1_im              : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL last_buffer_reg2_re              : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL last_buffer_reg2_im              : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL next_buffer_re                   : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL next_buffer_im                   : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL mult_results_re                  : vector_of_signed16(0 TO 3);  -- sfix16 [4]
  SIGNAL mult_results_im                  : vector_of_signed16(0 TO 3);  -- sfix16 [4]
  SIGNAL mult_results                     : vector_of_signed16(0 TO 3);  -- sfix16 [4]
  SIGNAL last_buffer_reg1_next_re         : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL last_buffer_reg1_next_im         : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL last_buffer_reg2_next_re         : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL last_buffer_reg2_next_im         : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL next_buffer_next_re              : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL next_buffer_next_im              : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL mult_results_re_next             : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL mult_results_im_next             : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]
  SIGNAL mult_results_next                : vector_of_signed16(0 TO 3);  -- sfix16_En14 [4]

BEGIN
  outputgen1: FOR k1 IN 0 TO 143 GENERATE
    buffer_re_signed(k1) <= signed(buffer_re(k1));
  END GENERATE;

  outputgen: FOR k1 IN 0 TO 143 GENERATE
    buffer_im_signed(k1) <= signed(buffer_im(k1));
  END GENERATE;

  align_index_unsigned <= unsigned(align_index);

  end_det_fn_FixPt_1_process : PROCESS (clk)
  BEGIN
    IF clk'EVENT AND clk = '1' THEN
      IF reset = '1' THEN
        last_buffer_reg1_re <= (OTHERS => to_signed(16#0000#, 16));
        last_buffer_reg1_im <= (OTHERS => to_signed(16#0000#, 16));
        last_buffer_reg2_re <= (OTHERS => to_signed(16#0000#, 16));
        last_buffer_reg2_im <= (OTHERS => to_signed(16#0000#, 16));
        next_buffer_re <= (OTHERS => to_signed(16#0000#, 16));
        next_buffer_im <= (OTHERS => to_signed(16#0000#, 16));
        mult_results_re <= (OTHERS => to_signed(16#0000#, 16));
        mult_results_im <= (OTHERS => to_signed(16#0000#, 16));
        mult_results <= (OTHERS => to_signed(16#0000#, 16));
      ELSIF enb = '1' THEN
        last_buffer_reg1_re <= last_buffer_reg1_next_re;
        last_buffer_reg1_im <= last_buffer_reg1_next_im;
        last_buffer_reg2_re <= last_buffer_reg2_next_re;
        last_buffer_reg2_im <= last_buffer_reg2_next_im;
        next_buffer_re <= next_buffer_next_re;
        next_buffer_im <= next_buffer_next_im;
        mult_results_re <= mult_results_re_next;
        mult_results_im <= mult_results_im_next;
        mult_results <= mult_results_next;
      END IF;
    END IF;
  END PROCESS end_det_fn_FixPt_1_process;

  end_det_fn_FixPt_1_output : PROCESS (align_index_unsigned, buffer_im_signed, buffer_re_signed, last_buffer_reg1_im,
       last_buffer_reg1_re, last_buffer_reg2_im, last_buffer_reg2_re,
       mult_results, mult_results_im, mult_results_re, next_buffer_im,
       next_buffer_re, valid)
    VARIABLE ii : unsigned(15 DOWNTO 0);
    VARIABLE Y : signed(17 DOWNTO 0);
    VARIABLE ii_1 : unsigned(15 DOWNTO 0);
    VARIABLE add_cast : vector_of_signed19(0 TO 2);
    VARIABLE add_cast_0 : vector_of_signed19(0 TO 2);
    VARIABLE add_temp : vector_of_signed19(0 TO 2);
    VARIABLE mul_temp : vector_of_signed32(0 TO 3);
    VARIABLE mul_temp_0 : vector_of_signed32(0 TO 3);
    VARIABLE add_temp_0 : vector_of_signed32(0 TO 3);
    VARIABLE sub_cast : vector_of_signed32(0 TO 3);
    VARIABLE add_cast_1 : vector_of_signed27(0 TO 3);
    VARIABLE add_temp_1 : vector_of_signed27(0 TO 3);
    VARIABLE add_cast_2 : vector_of_signed32(0 TO 3);
    VARIABLE add_cast_3 : vector_of_signed32(0 TO 3);
    VARIABLE add_temp_2 : vector_of_signed32(0 TO 3);
    VARIABLE sub_cast_0 : vector_of_signed32(0 TO 3);
    VARIABLE sub_cast_1 : vector_of_signed32(0 TO 3);
    VARIABLE add_cast_4 : vector_of_signed27(0 TO 3);
    VARIABLE add_temp_3 : vector_of_signed27(0 TO 3);
    VARIABLE add_cast_5 : vector_of_signed32(0 TO 3);
    VARIABLE add_cast_6 : vector_of_signed32(0 TO 3);
    VARIABLE add_temp_4 : vector_of_signed32(0 TO 3);
    VARIABLE sub_cast_2 : vector_of_signed32(0 TO 3);
    VARIABLE add_temp_5 : vector_of_signed32(0 TO 3);
    VARIABLE sub_cast_3 : vector_of_signed32(0 TO 3);
    VARIABLE add_cast_7 : vector_of_unsigned31(0 TO 3);
    VARIABLE add_cast_8 : vector_of_unsigned31(0 TO 3);
    VARIABLE add_temp_6 : vector_of_unsigned31(0 TO 3);
    VARIABLE sub_cast_4 : vector_of_signed32(0 TO 3);
    VARIABLE sub_cast_5 : vector_of_signed32(0 TO 3);
    VARIABLE add_cast_9 : vector_of_unsigned31(0 TO 3);
    VARIABLE add_cast_10 : vector_of_unsigned31(0 TO 3);
    VARIABLE add_temp_7 : vector_of_unsigned31(0 TO 3);
    VARIABLE sub_cast_6 : vector_of_signed32(0 TO 3);
  BEGIN
    last_buffer_reg1_next_re <= last_buffer_reg1_re;
    last_buffer_reg1_next_im <= last_buffer_reg1_im;
    last_buffer_reg2_next_re <= last_buffer_reg2_re;
    last_buffer_reg2_next_im <= last_buffer_reg2_im;
    next_buffer_next_re <= next_buffer_re;
    next_buffer_next_im <= next_buffer_im;
    --auto-generated
    -- end_detected=false;
    -- fpga_timesync_end_detected Check for 180 degree phase shift
    -- 802.11 protocols generally signal the end of the STF segment with a final
    -- STF repetition shifted by 180 degrees
    -- This module is pipelined into __ stages:
    -- 1. Collect buffer values
    -- 2. Complex multiplication, multiplication stage
    -- 3. Complex multiplication, addition stage
    -- 4. Sum (needs to be separate from 3 because we have max 2 adds per clk)
    IF valid = '1' THEN 
      Y := resize(mult_results(0), 18);

      FOR k IN 0 TO 2 LOOP
        add_cast(k) := resize(Y, 19);
        add_cast_0(k) := resize(mult_results(k + 1), 19);
        add_temp(k) := add_cast(k) + add_cast_0(k);
        Y := add_temp(k)(17 DOWNTO 0);
      END LOOP;

      end_detected <= hdlcoder_to_stdlogic(Y < to_signed(16#00000#, 18));
    ELSE 
      end_detected <= '0';
    END IF;
    -- next_buffer=complex(zeros(1,SAMPLE_WIDTH));
    -- for ii=1:SAMPLE_WIDTH
    --     next_buffer(ii) = buffer(128-SAMPLE_WIDTH+align_index+ii);
    -- end

    FOR t_0 IN 0 TO 3 LOOP
      mult_results_next(t_0) <= mult_results_re(t_0) + mult_results_im(t_0);
      mul_temp(t_0) := next_buffer_re(t_0) * last_buffer_reg2_re(t_0);
      mult_results_re_next(t_0) <= mul_temp(t_0)(29 DOWNTO 14);
      mul_temp_0(t_0) := next_buffer_im(t_0) * last_buffer_reg2_im(t_0);
      mult_results_im_next(t_0) <= mul_temp_0(t_0)(29 DOWNTO 14);
    END LOOP;

    IF valid = '1' THEN 
      next_buffer_next_re <= (OTHERS => to_signed(16#0000#, 16));
      next_buffer_next_im <= (OTHERS => to_signed(16#0000#, 16));

      FOR ii_0 IN 0 TO 3 LOOP
        add_temp_0(ii_0) := to_signed(ii_0 + 1, 32);
        ii := unsigned(add_temp_0(ii_0)(15 DOWNTO 0));
        sub_cast(ii_0) := signed(resize(ii, 32));
        add_cast_1(ii_0) := signed(resize(align_index_unsigned, 27));
        add_temp_1(ii_0) := to_signed(16#01F0000#, 27) + add_cast_1(ii_0);
        add_cast_2(ii_0) := resize(add_temp_1(ii_0), 32);
        add_cast_3(ii_0) := signed(resize(ii & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 32));
        add_temp_2(ii_0) := add_cast_2(ii_0) + add_cast_3(ii_0);
        sub_cast_0(ii_0) := resize(add_temp_2(ii_0)(31 DOWNTO 14), 32);
        next_buffer_next_re(to_integer(sub_cast(ii_0) - 1)) <= buffer_re_signed(to_integer(sub_cast_0(ii_0) - 1));
        sub_cast_1(ii_0) := signed(resize(ii, 32));
        add_cast_4(ii_0) := signed(resize(align_index_unsigned, 27));
        add_temp_3(ii_0) := to_signed(16#01F0000#, 27) + add_cast_4(ii_0);
        add_cast_5(ii_0) := resize(add_temp_3(ii_0), 32);
        add_cast_6(ii_0) := signed(resize(ii & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 32));
        add_temp_4(ii_0) := add_cast_5(ii_0) + add_cast_6(ii_0);
        sub_cast_2(ii_0) := resize(add_temp_4(ii_0)(31 DOWNTO 14), 32);
        next_buffer_next_im(to_integer(sub_cast_1(ii_0) - 1)) <= buffer_im_signed(to_integer(sub_cast_2(ii_0) - 1));
        last_buffer_reg2_next_re(ii_0) <= last_buffer_reg1_re(ii_0);
        last_buffer_reg2_next_im(ii_0) <= last_buffer_reg1_im(ii_0);
        add_temp_5(ii_0) := to_signed(ii_0 + 1, 32);
        ii_1 := unsigned(add_temp_5(ii_0)(15 DOWNTO 0));
        sub_cast_3(ii_0) := signed(resize(ii_1, 32));
        add_cast_7(ii_0) := resize(ii_1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 31);
        add_cast_8(ii_0) := resize(align_index_unsigned, 31);
        add_temp_6(ii_0) := add_cast_7(ii_0) + add_cast_8(ii_0);
        sub_cast_4(ii_0) := signed(resize(add_temp_6(ii_0)(30 DOWNTO 14), 32));
        last_buffer_reg1_next_re(to_integer(sub_cast_3(ii_0) - 1)) <= buffer_re_signed(to_integer(sub_cast_4(ii_0) - 1));
        sub_cast_5(ii_0) := signed(resize(ii_1, 32));
        add_cast_9(ii_0) := resize(ii_1 & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 31);
        add_cast_10(ii_0) := resize(align_index_unsigned, 31);
        add_temp_7(ii_0) := add_cast_9(ii_0) + add_cast_10(ii_0);
        sub_cast_6(ii_0) := signed(resize(add_temp_7(ii_0)(30 DOWNTO 14), 32));
        last_buffer_reg1_next_im(to_integer(sub_cast_5(ii_0) - 1)) <= buffer_im_signed(to_integer(sub_cast_6(ii_0) - 1));
      END LOOP;

    END IF;
  END PROCESS end_det_fn_FixPt_1_output;


END rtl;

