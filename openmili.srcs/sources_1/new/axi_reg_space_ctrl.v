`timescale 1ns / 1ps


module axi_reg_space_ctrl
  (
   //clock and reset
   input 	    S_AXI_ACLK,
   input 	    S_AXI_ARESETN,

   //write address channel
   input 	    S_AXI_AWVALID,
   output reg 	    S_AXI_AWREADY,
   
   //write data channel
   input 	    S_AXI_WVALID,
   output reg 	    S_AXI_WREADY,

   //write response channel
   output reg [1:0] S_AXI_BRESP,
   output reg 	    S_AXI_BVALID,
   input 	    S_AXI_BREADY,

   /*READ NOT IMPLEMENTED*/
   //read address channel
   input 	    S_AXI_ARVALID,
   output reg 	    S_AXI_ARREADY,

   //read data channel
   output reg [1:0] S_AXI_RRESP,
   output reg 	    S_AXI_RVALID,
   input 	    S_AXI_RREADY,


   //register space controls
   output reg 	    wr_reg = 1'b0
   
   );
 
   
   //read not implemented, for future 

   localparam
     ST_IDLE  = 2'b00,
     ST_WRITE = 2'b01,
     ST_READ  = 2'b11,
     ST_DONE  = 2'b10;
   
   reg [1:0]     n_st = 2'b0;
   reg [1:0]     c_st = 2'b0;
   
   always@(posedge S_AXI_ACLK)
     if(!S_AXI_ARESETN)
       c_st <= ST_IDLE;
     else
       c_st <= n_st;
   

   always@(/*AUTOSENSE*/S_AXI_ARVALID or S_AXI_AWVALID or S_AXI_BREADY
	   or S_AXI_RREADY or c_st)begin
      case(c_st)
        ST_IDLE : begin
           if(S_AXI_AWVALID) //assumes that AWVALID and WVALID arrive at the same time
             n_st <= ST_WRITE;
           else if(S_AXI_ARVALID)
	     n_st <= ST_READ;
	   else
             n_st <= ST_IDLE;   
        end
        
        ST_WRITE : begin
           if(S_AXI_BREADY)
             n_st <= ST_DONE;   
           else
             n_st <= ST_WRITE;
           
        end

	ST_READ : begin
	   if(S_AXI_RREADY)
	     n_st <= ST_DONE;
	   else
	     n_st <= ST_READ;	
	end

        ST_DONE : begin
           if(!S_AXI_AWVALID && !S_AXI_ARVALID)
             n_st <= ST_IDLE;
           else
             n_st <= ST_DONE;   
        end
      endcase // case (c_st)
      
   end // always@ (...
   
   

   always@(posedge S_AXI_ACLK)begin
      case(c_st)
        ST_IDLE : begin
           S_AXI_AWREADY <= 1'b0;
           S_AXI_WREADY  <= 1'b0;
           S_AXI_BVALID  <= 1'b0;
           S_AXI_BRESP   <= 2'b00;
           wr_reg        <= 1'b0;

	   S_AXI_ARREADY <= 1'b0;
	   S_AXI_RVALID  <= 1'b0;
	   S_AXI_RRESP   <= 2'b00; 
        end

        ST_WRITE : begin
           wr_reg        <= 1'b1;      
           S_AXI_AWREADY <= S_AXI_AWVALID;
           S_AXI_WREADY  <= S_AXI_WVALID;
           S_AXI_BVALID  <= 1'b1; 
           S_AXI_BRESP   <= 2'b00;
        end

	ST_READ : begin
	   S_AXI_ARREADY <= S_AXI_ARVALID;
	   S_AXI_RVALID  <= 1'b1;
	   S_AXI_RRESP   <= 2'b00;
	end

        ST_DONE : begin
           wr_reg        <= 1'b0;      
           
        end
      endcase // case (c_st)
   
   
   end
   
endmodule
