`timescale 1ns / 1ps





//AXI register space

module reg_space
  (
   //clock and reset
   input 	     S_AXI_ACLK,
   input 	     S_AXI_ARESETN,

   //write address channel
   input [31:0]      S_AXI_AWADDR,
   input 	     S_AXI_AWVALID,
   output 	     S_AXI_AWREADY,
   
   //write data channel
   input [31:0]      S_AXI_WDATA,
   input [3:0] 	     S_AXI_WSTRB,
   input 	     S_AXI_WVALID,
   output 	     S_AXI_WREADY,
   
   //write response channel
   output [1:0]      S_AXI_BRESP,
   output 	     S_AXI_BVALID,
   input 	     S_AXI_BREADY,
   
   //read address channel
   input [31:0]      S_AXI_ARADDR,
   input 	     S_AXI_ARVALID,
   output 	     S_AXI_ARREADY,

   //read data channel
   output reg [31:0] S_AXI_RDATA,
   output [1:0]      S_AXI_RRESP,
   output 	     S_AXI_RVALID,
   input 	     S_AXI_RREADY
   

   );
   
   (* mark_debug = "true"*) wire 	wr_reg;

   (* mark_debug = "true"*) reg [31:0] reg_test1;
   (* mark_debug = "true"*) reg [31:0] reg_test2;
   
   axi_reg_space_ctrl u_axi_reg_space_ctrl
     (/*AUTOINST*/
      // Outputs
      .S_AXI_AWREADY			(S_AXI_AWREADY),
      .S_AXI_WREADY			(S_AXI_WREADY),
      .S_AXI_BRESP			(S_AXI_BRESP[1:0]),
      .S_AXI_BVALID			(S_AXI_BVALID),
      .S_AXI_ARREADY			(S_AXI_ARREADY),
      .S_AXI_RRESP			(S_AXI_RRESP[1:0]),
      .S_AXI_RVALID			(S_AXI_RVALID),
      .wr_reg				(wr_reg),
      // Inputs
      .S_AXI_ACLK			(S_AXI_ACLK),
      .S_AXI_ARESETN			(S_AXI_ARESETN),
      .S_AXI_AWVALID			(S_AXI_AWVALID),
      .S_AXI_WVALID			(S_AXI_WVALID),
      .S_AXI_BREADY			(S_AXI_BREADY),
      .S_AXI_ARVALID			(S_AXI_ARVALID),
      .S_AXI_RREADY			(S_AXI_RREADY));

   wire [31:0] 	     data;
   
   assign data = {S_AXI_WDATA[31:24]&{8{S_AXI_WSTRB[3]}},
                  S_AXI_WDATA[23:16]&{8{S_AXI_WSTRB[2]}},
		  S_AXI_WDATA[15:08]&{8{S_AXI_WSTRB[1]}},
		  S_AXI_WDATA[07:00]&{8{S_AXI_WSTRB[0]}}};
			  
							  
   always@(posedge S_AXI_ACLK)
     if(!S_AXI_ARESETN)begin
	reg_test1 <= 32'h0123_4567;
	reg_test2 <= 32'h89AB_CDEF;    
     end
     else if(wr_reg)
       case(S_AXI_AWADDR) //address is set from the address editor
	 32'h0000_0000 : reg_test1 <= data; 
	 32'h0000_0004 : reg_test2 <= data;
       endcase // case (address)


   always@(posedge S_AXI_ACLK)
     if(!S_AXI_ARESETN)begin
	S_AXI_RDATA <= 32'd0;	
     end
     else if(S_AXI_ARVALID)
       case(S_AXI_ARADDR)
	 32'h0000_0000 : S_AXI_RDATA <= reg_test1;
	 32'h0000_0004 : S_AXI_RDATA <= reg_test2;
       endcase // case (address)
	 

   
endmodule
