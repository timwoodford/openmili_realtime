//Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2018.1 (win64) Build 2188600 Wed Apr  4 18:40:38 MDT 2018
//Date        : Wed Oct 17 10:59:54 2018
//Host        : DESKTOP-IT65841 running 64-bit major release  (build 9200)
//Command     : generate_target system_wrapper.bd
//Design      : system_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module system_wrapper
   (M_AXI_LITE_0_araddr,
    M_AXI_LITE_0_arprot,
    M_AXI_LITE_0_arready,
    M_AXI_LITE_0_arvalid,
    M_AXI_LITE_0_awaddr,
    M_AXI_LITE_0_awprot,
    M_AXI_LITE_0_awready,
    M_AXI_LITE_0_awvalid,
    M_AXI_LITE_0_bready,
    M_AXI_LITE_0_bresp,
    M_AXI_LITE_0_bvalid,
    M_AXI_LITE_0_rdata,
    M_AXI_LITE_0_rready,
    M_AXI_LITE_0_rresp,
    M_AXI_LITE_0_rvalid,
    M_AXI_LITE_0_wdata,
    M_AXI_LITE_0_wready,
    M_AXI_LITE_0_wstrb,
    M_AXI_LITE_0_wvalid,
    c0_ddr4_act_n,
    c0_ddr4_adr,
    c0_ddr4_ba,
    c0_ddr4_bg,
    c0_ddr4_ck_c,
    c0_ddr4_ck_t,
    c0_ddr4_cke,
    c0_ddr4_cs_n,
    c0_ddr4_dm_n,
    c0_ddr4_dq,
    c0_ddr4_dqs_c,
    c0_ddr4_dqs_t,
    c0_ddr4_odt,
    c0_ddr4_reset_n,
    gpio0_i,
    gpio0_o,
    gpio0_t,
    gpio1_i,
    gpio1_o,
    gpio1_t,
    mb_intr_4,
    mb_intr_5,
    pci_express_x8_rxn,
    pci_express_x8_rxp,
    pci_express_x8_txn,
    pci_express_x8_txp,
    pcie_perstn,
    pcie_refclk_clk_n,
    pcie_refclk_clk_p,
    rx_data_0_n,
    rx_data_0_p,
    rx_data_1_n,
    rx_data_1_p,
    rx_data_2_n,
    rx_data_2_p,
    rx_data_3_n,
    rx_data_3_p,
    rx_ref_clk_0,
    rx_sync_0,
    rx_sysref_0,
    spi_clk_i,
    spi_clk_o,
    spi_csn_i,
    spi_csn_o,
    spi_sdi_i,
    spi_sdo_i,
    spi_sdo_o,
    sys_clk_clk_n,
    sys_clk_clk_p,
    sys_rst,
    tx_data_0_n,
    tx_data_0_p,
    tx_data_1_n,
    tx_data_1_p,
    tx_data_2_n,
    tx_data_2_p,
    tx_data_3_n,
    tx_data_3_p,
    tx_ref_clk_0,
    tx_sync_0,
    tx_sysref_0,
    uart_sin,
    uart_sout);
  output [31:0]M_AXI_LITE_0_araddr;
  output [2:0]M_AXI_LITE_0_arprot;
  input M_AXI_LITE_0_arready;
  output M_AXI_LITE_0_arvalid;
  output [31:0]M_AXI_LITE_0_awaddr;
  output [2:0]M_AXI_LITE_0_awprot;
  input M_AXI_LITE_0_awready;
  output M_AXI_LITE_0_awvalid;
  output M_AXI_LITE_0_bready;
  input [1:0]M_AXI_LITE_0_bresp;
  input M_AXI_LITE_0_bvalid;
  input [31:0]M_AXI_LITE_0_rdata;
  output M_AXI_LITE_0_rready;
  input [1:0]M_AXI_LITE_0_rresp;
  input M_AXI_LITE_0_rvalid;
  output [31:0]M_AXI_LITE_0_wdata;
  input M_AXI_LITE_0_wready;
  output [3:0]M_AXI_LITE_0_wstrb;
  output M_AXI_LITE_0_wvalid;
  output c0_ddr4_act_n;
  output [16:0]c0_ddr4_adr;
  output [1:0]c0_ddr4_ba;
  output c0_ddr4_bg;
  output c0_ddr4_ck_c;
  output c0_ddr4_ck_t;
  output c0_ddr4_cke;
  output c0_ddr4_cs_n;
  inout [7:0]c0_ddr4_dm_n;
  inout [63:0]c0_ddr4_dq;
  inout [7:0]c0_ddr4_dqs_c;
  inout [7:0]c0_ddr4_dqs_t;
  output c0_ddr4_odt;
  output c0_ddr4_reset_n;
  input [31:0]gpio0_i;
  output [31:0]gpio0_o;
  output [31:0]gpio0_t;
  input [31:0]gpio1_i;
  output [31:0]gpio1_o;
  output [31:0]gpio1_t;
  input mb_intr_4;
  input mb_intr_5;
  input [7:0]pci_express_x8_rxn;
  input [7:0]pci_express_x8_rxp;
  output [7:0]pci_express_x8_txn;
  output [7:0]pci_express_x8_txp;
  input pcie_perstn;
  input pcie_refclk_clk_n;
  input pcie_refclk_clk_p;
  input rx_data_0_n;
  input rx_data_0_p;
  input rx_data_1_n;
  input rx_data_1_p;
  input rx_data_2_n;
  input rx_data_2_p;
  input rx_data_3_n;
  input rx_data_3_p;
  input rx_ref_clk_0;
  output [0:0]rx_sync_0;
  input rx_sysref_0;
  input spi_clk_i;
  output spi_clk_o;
  input [7:0]spi_csn_i;
  output [7:0]spi_csn_o;
  input spi_sdi_i;
  input spi_sdo_i;
  output spi_sdo_o;
  input sys_clk_clk_n;
  input sys_clk_clk_p;
  input sys_rst;
  output tx_data_0_n;
  output tx_data_0_p;
  output tx_data_1_n;
  output tx_data_1_p;
  output tx_data_2_n;
  output tx_data_2_p;
  output tx_data_3_n;
  output tx_data_3_p;
  input tx_ref_clk_0;
  input tx_sync_0;
  input tx_sysref_0;
  input uart_sin;
  output uart_sout;

  wire [31:0]M_AXI_LITE_0_araddr;
  wire [2:0]M_AXI_LITE_0_arprot;
  wire M_AXI_LITE_0_arready;
  wire M_AXI_LITE_0_arvalid;
  wire [31:0]M_AXI_LITE_0_awaddr;
  wire [2:0]M_AXI_LITE_0_awprot;
  wire M_AXI_LITE_0_awready;
  wire M_AXI_LITE_0_awvalid;
  wire M_AXI_LITE_0_bready;
  wire [1:0]M_AXI_LITE_0_bresp;
  wire M_AXI_LITE_0_bvalid;
  wire [31:0]M_AXI_LITE_0_rdata;
  wire M_AXI_LITE_0_rready;
  wire [1:0]M_AXI_LITE_0_rresp;
  wire M_AXI_LITE_0_rvalid;
  wire [31:0]M_AXI_LITE_0_wdata;
  wire M_AXI_LITE_0_wready;
  wire [3:0]M_AXI_LITE_0_wstrb;
  wire M_AXI_LITE_0_wvalid;
  wire c0_ddr4_act_n;
  wire [16:0]c0_ddr4_adr;
  wire [1:0]c0_ddr4_ba;
  wire c0_ddr4_bg;
  wire c0_ddr4_ck_c;
  wire c0_ddr4_ck_t;
  wire c0_ddr4_cke;
  wire c0_ddr4_cs_n;
  wire [7:0]c0_ddr4_dm_n;
  wire [63:0]c0_ddr4_dq;
  wire [7:0]c0_ddr4_dqs_c;
  wire [7:0]c0_ddr4_dqs_t;
  wire c0_ddr4_odt;
  wire c0_ddr4_reset_n;
  wire [31:0]gpio0_i;
  wire [31:0]gpio0_o;
  wire [31:0]gpio0_t;
  wire [31:0]gpio1_i;
  wire [31:0]gpio1_o;
  wire [31:0]gpio1_t;
  wire mb_intr_4;
  wire mb_intr_5;
  wire [7:0]pci_express_x8_rxn;
  wire [7:0]pci_express_x8_rxp;
  wire [7:0]pci_express_x8_txn;
  wire [7:0]pci_express_x8_txp;
  wire pcie_perstn;
  wire pcie_refclk_clk_n;
  wire pcie_refclk_clk_p;
  wire rx_data_0_n;
  wire rx_data_0_p;
  wire rx_data_1_n;
  wire rx_data_1_p;
  wire rx_data_2_n;
  wire rx_data_2_p;
  wire rx_data_3_n;
  wire rx_data_3_p;
  wire rx_ref_clk_0;
  wire [0:0]rx_sync_0;
  wire rx_sysref_0;
  wire spi_clk_i;
  wire spi_clk_o;
  wire [7:0]spi_csn_i;
  wire [7:0]spi_csn_o;
  wire spi_sdi_i;
  wire spi_sdo_i;
  wire spi_sdo_o;
  wire sys_clk_clk_n;
  wire sys_clk_clk_p;
  wire sys_rst;
  wire tx_data_0_n;
  wire tx_data_0_p;
  wire tx_data_1_n;
  wire tx_data_1_p;
  wire tx_data_2_n;
  wire tx_data_2_p;
  wire tx_data_3_n;
  wire tx_data_3_p;
  wire tx_ref_clk_0;
  wire tx_sync_0;
  wire tx_sysref_0;
  wire uart_sin;
  wire uart_sout;

  system system_i
       (.M_AXI_LITE_0_araddr(M_AXI_LITE_0_araddr),
        .M_AXI_LITE_0_arprot(M_AXI_LITE_0_arprot),
        .M_AXI_LITE_0_arready(M_AXI_LITE_0_arready),
        .M_AXI_LITE_0_arvalid(M_AXI_LITE_0_arvalid),
        .M_AXI_LITE_0_awaddr(M_AXI_LITE_0_awaddr),
        .M_AXI_LITE_0_awprot(M_AXI_LITE_0_awprot),
        .M_AXI_LITE_0_awready(M_AXI_LITE_0_awready),
        .M_AXI_LITE_0_awvalid(M_AXI_LITE_0_awvalid),
        .M_AXI_LITE_0_bready(M_AXI_LITE_0_bready),
        .M_AXI_LITE_0_bresp(M_AXI_LITE_0_bresp),
        .M_AXI_LITE_0_bvalid(M_AXI_LITE_0_bvalid),
        .M_AXI_LITE_0_rdata(M_AXI_LITE_0_rdata),
        .M_AXI_LITE_0_rready(M_AXI_LITE_0_rready),
        .M_AXI_LITE_0_rresp(M_AXI_LITE_0_rresp),
        .M_AXI_LITE_0_rvalid(M_AXI_LITE_0_rvalid),
        .M_AXI_LITE_0_wdata(M_AXI_LITE_0_wdata),
        .M_AXI_LITE_0_wready(M_AXI_LITE_0_wready),
        .M_AXI_LITE_0_wstrb(M_AXI_LITE_0_wstrb),
        .M_AXI_LITE_0_wvalid(M_AXI_LITE_0_wvalid),
        .c0_ddr4_act_n(c0_ddr4_act_n),
        .c0_ddr4_adr(c0_ddr4_adr),
        .c0_ddr4_ba(c0_ddr4_ba),
        .c0_ddr4_bg(c0_ddr4_bg),
        .c0_ddr4_ck_c(c0_ddr4_ck_c),
        .c0_ddr4_ck_t(c0_ddr4_ck_t),
        .c0_ddr4_cke(c0_ddr4_cke),
        .c0_ddr4_cs_n(c0_ddr4_cs_n),
        .c0_ddr4_dm_n(c0_ddr4_dm_n),
        .c0_ddr4_dq(c0_ddr4_dq),
        .c0_ddr4_dqs_c(c0_ddr4_dqs_c),
        .c0_ddr4_dqs_t(c0_ddr4_dqs_t),
        .c0_ddr4_odt(c0_ddr4_odt),
        .c0_ddr4_reset_n(c0_ddr4_reset_n),
        .gpio0_i(gpio0_i),
        .gpio0_o(gpio0_o),
        .gpio0_t(gpio0_t),
        .gpio1_i(gpio1_i),
        .gpio1_o(gpio1_o),
        .gpio1_t(gpio1_t),
        .mb_intr_4(mb_intr_4),
        .mb_intr_5(mb_intr_5),
        .pci_express_x8_rxn(pci_express_x8_rxn),
        .pci_express_x8_rxp(pci_express_x8_rxp),
        .pci_express_x8_txn(pci_express_x8_txn),
        .pci_express_x8_txp(pci_express_x8_txp),
        .pcie_perstn(pcie_perstn),
        .pcie_refclk_clk_n(pcie_refclk_clk_n),
        .pcie_refclk_clk_p(pcie_refclk_clk_p),
        .rx_data_0_n(rx_data_0_n),
        .rx_data_0_p(rx_data_0_p),
        .rx_data_1_n(rx_data_1_n),
        .rx_data_1_p(rx_data_1_p),
        .rx_data_2_n(rx_data_2_n),
        .rx_data_2_p(rx_data_2_p),
        .rx_data_3_n(rx_data_3_n),
        .rx_data_3_p(rx_data_3_p),
        .rx_ref_clk_0(rx_ref_clk_0),
        .rx_sync_0(rx_sync_0),
        .rx_sysref_0(rx_sysref_0),
        .spi_clk_i(spi_clk_i),
        .spi_clk_o(spi_clk_o),
        .spi_csn_i(spi_csn_i),
        .spi_csn_o(spi_csn_o),
        .spi_sdi_i(spi_sdi_i),
        .spi_sdo_i(spi_sdo_i),
        .spi_sdo_o(spi_sdo_o),
        .sys_clk_clk_n(sys_clk_clk_n),
        .sys_clk_clk_p(sys_clk_clk_p),
        .sys_rst(sys_rst),
        .tx_data_0_n(tx_data_0_n),
        .tx_data_0_p(tx_data_0_p),
        .tx_data_1_n(tx_data_1_n),
        .tx_data_1_p(tx_data_1_p),
        .tx_data_2_n(tx_data_2_n),
        .tx_data_2_p(tx_data_2_p),
        .tx_data_3_n(tx_data_3_n),
        .tx_data_3_p(tx_data_3_p),
        .tx_ref_clk_0(tx_ref_clk_0),
        .tx_sync_0(tx_sync_0),
        .tx_sysref_0(tx_sysref_0),
        .uart_sin(uart_sin),
        .uart_sout(uart_sout));
endmodule
