/*
 * openmili.c
 *
 *  Created on: Jul 17, 2018
 *      Author: fencerex
 */

#include "platform_drivers.h"
#include "ad9523.h"
#include "ad9680.h"
#include "ad9144.h"
#include "adc_core.h"
#include "dac_core.h"
#include "dmac_core.h"
#include "dac_buffer.h"
#include "xcvr_core.h"
#include "jesd_core.h"
#include "xintc.h"

#define GPIO_TRIG		    43
#define GPIO_ADC_PD		    42
#define GPIO_DAC_TXEN		41
#define GPIO_DAC_RESET		40
#define GPIO_CLKD_SYNC		38
#define GPIO_ADC_FDB		36
#define GPIO_ADC_FDA		35
#define GPIO_DAC_IRQ		34
#define GPIO_CLKD_STATUS_1	33
#define GPIO_CLKD_STATUS_0	32

#define DMA_BUFFER		 0

enum ad9523_channels {
	DAC_DEVICE_CLK,
	DAC_DEVICE_SYSREF,
	DAC_FPGA_CLK,
	DAC_FPGA_SYSREF,
	ADC_DEVICE_CLK,
	ADC_DEVICE_SYSREF,
	ADC_FPGA_CLK,
	ADC_FPGA_SYSREF
};

int fmcdaq2_reconfig(struct ad9144_init_param *p_ad9144_param,
					 xcvr_core *p_ad9144_xcvr,
					 struct ad9680_init_param *p_ad9680_param,
			 	 	 xcvr_core *p_ad9680_xcvr,
					 struct ad9523_platform_data *p_ad9523_param)
{

	uint8_t mode = 0;

	printf ("Available sampling rates:\n");
	printf ("\t1 - ADC 1000 MSPS; DAC 1000 MSPS\n");
	printf ("\t2 - ADC  500 MSPS; DAC 1000 MSPS\n");
	printf ("\t3 - ADC  500 MSPS; DAC  500 MSPS\n");
	printf ("\t4 - ADC  600 MSPS; DAC  600 MSPS\n");
	printf ("choose an option [default 1]:\n");

	mode = ad_uart_read();

	switch (mode) {
		case '4':
			printf ("4 - ADC  600 MSPS; DAC  600 MSPS\n");
			p_ad9523_param->pll2_vco_diff_m1 = 5;
			(&p_ad9523_param->channels[DAC_FPGA_CLK])->
					channel_divider = 2;
			(&p_ad9523_param->channels[DAC_DEVICE_CLK])->
					channel_divider = 1;
			(&p_ad9523_param->channels[DAC_DEVICE_SYSREF])->
					channel_divider = 128;
			(&p_ad9523_param->channels[DAC_FPGA_SYSREF])->
					channel_divider = 128;
			(&p_ad9523_param->channels[ADC_FPGA_CLK])->
					channel_divider = 2;
			(&p_ad9523_param->channels[ADC_DEVICE_CLK])->
					channel_divider = 1;
			(&p_ad9523_param->channels[ADC_DEVICE_SYSREF])->
					channel_divider = 128;
			(&p_ad9523_param->channels[ADC_FPGA_SYSREF])->
					channel_divider = 128;
			p_ad9144_xcvr->reconfig_bypass = 0;
			p_ad9144_param->lane_rate_kbps = 6000000;
			p_ad9144_xcvr->lane_rate_kbps = 6000000;
			p_ad9144_xcvr->ref_clock_khz = 300000;
			p_ad9680_xcvr->reconfig_bypass = 0;
			p_ad9680_param->lane_rate_kbps = 6000000;
			p_ad9680_xcvr->lane_rate_kbps = 6000000;
			p_ad9680_xcvr->ref_clock_khz = 300000;

			p_ad9144_xcvr->dev.lpm_enable = 0;
			p_ad9144_xcvr->dev.qpll_enable = 0;
			p_ad9144_xcvr->dev.out_clk_sel = 4;

			p_ad9680_xcvr->dev.lpm_enable = 1;
			p_ad9680_xcvr->dev.qpll_enable = 0;
			p_ad9680_xcvr->dev.out_clk_sel = 4;

			break;
		case '3':
			printf ("3 - ADC  500 MSPS; DAC  500 MSPS\n");
			p_ad9523_param->pll2_vco_diff_m1 = 3;
			(&p_ad9523_param->channels[DAC_FPGA_CLK])->
					channel_divider = 4;
			(&p_ad9523_param->channels[DAC_DEVICE_CLK])->
					channel_divider = 2;
			(&p_ad9523_param->channels[DAC_DEVICE_SYSREF])->
					channel_divider = 256;
			(&p_ad9523_param->channels[DAC_FPGA_SYSREF])->
					channel_divider = 256;
			(&p_ad9523_param->channels[ADC_FPGA_CLK])->
					channel_divider = 4;
			(&p_ad9523_param->channels[ADC_DEVICE_CLK])->
					channel_divider = 2;
			(&p_ad9523_param->channels[ADC_DEVICE_SYSREF])->
					channel_divider = 256;
			(&p_ad9523_param->channels[ADC_FPGA_SYSREF])->
					channel_divider = 256;
			p_ad9144_xcvr->reconfig_bypass = 0;
			p_ad9144_param->lane_rate_kbps = 5000000;
			p_ad9144_xcvr->lane_rate_kbps = 5000000;
			p_ad9144_xcvr->ref_clock_khz = 250000;
			p_ad9680_xcvr->reconfig_bypass = 0;
			p_ad9680_param->lane_rate_kbps = 5000000;
			p_ad9680_xcvr->lane_rate_kbps = 5000000;
			p_ad9680_xcvr->ref_clock_khz = 250000;

			p_ad9144_xcvr->dev.lpm_enable = 1;
			p_ad9144_xcvr->dev.qpll_enable = 0;
			p_ad9144_xcvr->dev.out_clk_sel = 4;

			p_ad9680_xcvr->dev.lpm_enable = 1;
			p_ad9680_xcvr->dev.qpll_enable = 0;
			p_ad9680_xcvr->dev.out_clk_sel = 4;

			break;
		case '2':
			printf ("2 - ADC  500 MSPS; DAC 1000 MSPS\n");
			p_ad9523_param->pll2_vco_diff_m1 = 3;
			(&p_ad9523_param->channels[DAC_FPGA_CLK])->
					channel_divider = 2;
			(&p_ad9523_param->channels[DAC_DEVICE_CLK])->
					channel_divider = 1;
			(&p_ad9523_param->channels[DAC_DEVICE_SYSREF])->
					channel_divider = 128;
			(&p_ad9523_param->channels[DAC_FPGA_SYSREF])->
					channel_divider = 128;
			(&p_ad9523_param->channels[ADC_FPGA_CLK])->
					channel_divider = 4;
			(&p_ad9523_param->channels[ADC_DEVICE_CLK])->
					channel_divider = 2;
			(&p_ad9523_param->channels[ADC_DEVICE_SYSREF])->
					channel_divider = 256;
			(&p_ad9523_param->channels[ADC_FPGA_SYSREF])->
					channel_divider = 256;
			p_ad9144_xcvr->reconfig_bypass = 0;
			p_ad9144_param->lane_rate_kbps = 10000000;
			p_ad9144_xcvr->lane_rate_kbps = 10000000;
			p_ad9144_xcvr->ref_clock_khz = 500000;
			p_ad9680_xcvr->reconfig_bypass = 0;
			p_ad9680_param->lane_rate_kbps = 5000000;
			p_ad9680_xcvr->lane_rate_kbps = 5000000;
			p_ad9680_xcvr->ref_clock_khz = 250000;

			p_ad9144_xcvr->dev.lpm_enable = 0;
			p_ad9144_xcvr->dev.qpll_enable = 1;
			p_ad9144_xcvr->dev.out_clk_sel = 4;

			p_ad9680_xcvr->dev.lpm_enable = 1;
			p_ad9680_xcvr->dev.qpll_enable = 0;
			p_ad9680_xcvr->dev.out_clk_sel = 4;

			break;
		default:
			printf ("1 - ADC 1000 MSPS; DAC 1000 MSPS\n");
			p_ad9144_xcvr->ref_clock_khz = 500000;
			p_ad9680_xcvr->ref_clock_khz = 500000;
			break;
	}

	return(0);
}
volatile static int InterruptProcessed = FALSE;

//Reconfigure setup
void UserIntc4(void *CallBackRef)
{

	InterruptProcessed = TRUE;
}


void UserIntc5(void *CallBackRef)
{

}
int main(void)
{
	int intc_status;
	static XIntc InterruptController;
	intc_status = XIntc_Initialize(&InterruptController, XPAR_INTC_0_DEVICE_ID);
	if (intc_status != XST_SUCCESS) {xil_printf("Interrupt Initialization Failed\r\n");return XST_FAILURE;}

	//intc_status = XIntc_SelfTest(&InterruptController);
	//if (intc_status != XST_SUCCESS) {xil_printf("Self Test Failed\r\n");return XST_FAILURE;}

	intc_status = XIntc_Connect(&InterruptController, XPAR_AXI_INTC_SYSTEM_MB_INTR_4_INTR,
							(XInterruptHandler)UserIntc4, (void *)&InterruptController);
	if (intc_status != XST_SUCCESS) {xil_printf("Handler Connect Failed\r\n");return XST_FAILURE;}

	//Start Interrupt in real mode
	//intc_status = XIntc_Start(&InterruptController,XIN_SIMULATION_MODE);
	intc_status = XIntc_Start(&InterruptController,XIN_REAL_MODE);
	if (intc_status != XST_SUCCESS) { xil_printf("Handler Connect Failed\r\n");return XST_FAILURE;}

	//Enable the interrupt for the device
	XIntc_Enable(&InterruptController, XPAR_AXI_INTC_SYSTEM_MB_INTR_4_INTR);
	//Initialize the exception table
	Xil_ExceptionInit();
	//Register the interrupt controller handler with the exception table
	Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler)XIntc_InterruptHandler,&InterruptController);

	Xil_ExceptionEnable();

	//intc_status  = XIntc_SimulateIntr(&InterruptController, XPAR_AXI_INTC_SYSTEM_MB_INTR_4_INTR);
	/*printf("Waiting for Interrupt\n");
	while(1)
	{
		if(InterruptProcessed){
			printf("Interrupt Successful\n");
			break;
		}
	}
*/
	dac_core		ad9144_core;
	dac_channel		ad9144_channels[2];
	jesd_core		ad9144_jesd;
	//dmac_core		ad9144_dma;
	xcvr_core		ad9144_xcvr;
	adc_core		ad9680_core;
	jesd_core		ad9680_jesd;
	xcvr_core		ad9680_xcvr;
	//dmac_core       ad9680_dma;
	//dmac_xfer       rx_xfer;
	//dmac_xfer       tx_xfer;

	printf("Start Configuration\n");
	//Set Up SPI protocols
	spi_init_param	ad9523_spi_param;
	spi_init_param  ad9144_spi_param;
	spi_init_param  ad9680_spi_param;

	ad9523_spi_param.type = MICROBLAZE_SPI;
	ad9523_spi_param.chip_select = 0x6;
	ad9523_spi_param.cpha = 0;
	ad9523_spi_param.cpol = 0;

	ad9144_spi_param.type = MICROBLAZE_SPI;
	ad9144_spi_param.chip_select = 0x5;
	ad9144_spi_param.cpha = 0;
	ad9144_spi_param.cpol = 0;

	ad9680_spi_param.type = MICROBLAZE_SPI;
	ad9680_spi_param.chip_select = 0x3;
	ad9680_spi_param.cpha = 0;
	ad9680_spi_param.cpol = 0;

	struct ad9523_channel_spec	ad9523_channels[8];
	struct ad9523_platform_data	ad9523_pdata;
	struct ad9523_init_param	ad9523_param;
	struct ad9144_init_param	ad9144_param;
	struct ad9680_init_param	ad9680_param;

	ad9523_param.spi_init = ad9523_spi_param;
	ad9144_param.spi_init = ad9144_spi_param;
	ad9680_param.spi_init = ad9680_spi_param;

	struct ad9523_dev *ad9523_device;
	struct ad9144_dev *ad9144_device;
	struct ad9680_dev *ad9680_device;


	ad9144_xcvr.base_address = XPAR_AXI_AD9144_XCVR_BASEADDR;
	ad9144_core.base_address = XPAR_AXI_AD9144_CORE_BASEADDR;
	ad9144_jesd.base_address = XPAR_AXI_AD9144_JESD_TX_AXI_BASEADDR;
	//ad9144_dma.base_address  = XPAR_AXI_AD9144_DMA_BASEADDR;
	ad9680_xcvr.base_address = XPAR_AXI_AD9680_XCVR_BASEADDR;
	ad9680_core.base_address = XPAR_AXI_AD9680_CORE_BASEADDR;
	ad9680_jesd.base_address = XPAR_AXI_AD9680_JESD_RX_AXI_BASEADDR;
	//ad9680_dma.base_address  = XPAR_AXI_AD9680_DMA_BASEADDR;

	//rx_xfer.start_address = XPAR_AXI_DDR_CNTRL_BASEADDR + 0x000000;
	//tx_xfer.start_address = XPAR_AXI_DDR_CNTRL_BASEADDR + 0x000000;

	ad9523_pdata.num_channels = 8;
	ad9523_pdata.channels = &ad9523_channels[0];
	ad9523_param.pdata = &ad9523_pdata;
	ad9523_init(&ad9523_param);

	ad9523_channels[DAC_DEVICE_CLK].channel_num = 1;
	ad9523_channels[DAC_DEVICE_CLK].channel_divider = 1;
	ad9523_channels[DAC_DEVICE_SYSREF].channel_num = 7;
	ad9523_channels[DAC_DEVICE_SYSREF].channel_divider = 128;
	ad9523_channels[DAC_FPGA_CLK].channel_num = 9;
	ad9523_channels[DAC_FPGA_CLK].channel_divider = 2;
	ad9523_channels[DAC_FPGA_SYSREF].channel_num = 8;
	ad9523_channels[DAC_FPGA_SYSREF].channel_divider = 128;

	ad9523_channels[ADC_DEVICE_CLK].channel_num = 13;
	ad9523_channels[ADC_DEVICE_CLK].channel_divider = 1;
	ad9523_channels[ADC_DEVICE_SYSREF].channel_num = 6;
	ad9523_channels[ADC_DEVICE_SYSREF].channel_divider = 128;
	ad9523_channels[ADC_FPGA_CLK].channel_num = 4;
	ad9523_channels[ADC_FPGA_CLK].channel_divider = 2;
	ad9523_channels[ADC_FPGA_SYSREF].channel_num = 5;
	ad9523_channels[ADC_FPGA_SYSREF].channel_divider = 128;

	// VCXO 125MHz

	ad9523_pdata.vcxo_freq = 125000000;
	ad9523_pdata.spi3wire = 1;
	ad9523_pdata.osc_in_diff_en = 1;
	ad9523_pdata.pll2_charge_pump_current_nA = 413000;
	ad9523_pdata.pll2_freq_doubler_en = 0;
	ad9523_pdata.pll2_r2_div = 1;
	ad9523_pdata.pll2_ndiv_a_cnt = 0;
	ad9523_pdata.pll2_ndiv_b_cnt = 6;
	ad9523_pdata.pll2_vco_diff_m1 = 3;
	ad9523_pdata.pll2_vco_diff_m2 = 0;
	ad9523_pdata.rpole2 = 0;
	ad9523_pdata.rzero = 7;
	ad9523_pdata.cpole1 = 2;

	ad9144_xcvr.ref_clock_khz = 500000;
	ad9680_xcvr.ref_clock_khz = 500000;

	//******************************************************************************
	// DAC (AD9144) and the transmit path (AXI_ADXCVR,
	//	JESD204, AXI_AD9144, TX DMAC) configuration
	//******************************************************************************

	xcvr_getconfig(&ad9144_xcvr);
	ad9144_xcvr.reconfig_bypass = 1;
	ad9144_xcvr.dev.qpll_enable = 1;
	ad9144_xcvr.lane_rate_kbps = 10000000;

	ad9144_jesd.rx_tx_n = 0;
	ad9144_jesd.scramble_enable = 1;
	ad9144_jesd.octets_per_frame = 1;
	ad9144_jesd.frames_per_multiframe = 32;
	ad9144_jesd.subclass_mode = 1;

	ad9144_channels[0].dds_dual_tone = 0;
	ad9144_channels[0].dds_frequency_0 = 250*1000*1000; //100 MHz sine wave
	ad9144_channels[0].dds_phase_0 = 0;
	ad9144_channels[0].dds_scale_0 = 500000; //in microunits, so this is
	ad9144_channels[0].pat_data = 0xb1b0a1a0;
	ad9144_channels[0].sel = DAC_SRC_DDS;

	ad9144_channels[1].dds_dual_tone = 0;
	ad9144_channels[1].dds_frequency_0 = 250*1000*1000;
	ad9144_channels[1].dds_phase_0 = 0;
	ad9144_channels[1].dds_scale_0 = 500000;
	ad9144_channels[1].pat_data = 0xd1d0d1d0; //originally d1d0 c1c0
	ad9144_channels[1].sel = DAC_SRC_DDS;

	//init param
	ad9144_param.lane_rate_kbps = 10000000;
	ad9144_param.active_converters = 2;

	ad9144_core.no_of_channels = ad9144_param.active_converters;
	ad9144_core.resolution = 16;
	ad9144_core.channels = &ad9144_channels[0];

	ad9144_param.stpl_samples[0][0] =
			(ad9144_channels[0].pat_data >> 0)  & 0xffff;
	ad9144_param.stpl_samples[0][1] =
			(ad9144_channels[0].pat_data >> 16) & 0xffff;
	ad9144_param.stpl_samples[0][2] =
			(ad9144_channels[0].pat_data >> 0)  & 0xffff;
	ad9144_param.stpl_samples[0][3] =
			(ad9144_channels[0].pat_data >> 16) & 0xffff;
	ad9144_param.stpl_samples[1][0] =
			(ad9144_channels[1].pat_data >> 0)  & 0xffff;
	ad9144_param.stpl_samples[1][1] =
			(ad9144_channels[1].pat_data >> 16) & 0xffff;
	ad9144_param.stpl_samples[1][2] =
			(ad9144_channels[1].pat_data >> 0)  & 0xffff;
	ad9144_param.stpl_samples[1][3] =
			(ad9144_channels[1].pat_data >> 16) & 0xffff;


	//******************************************************************************
	// ADC (AD9680) and the receive path ( AXI_ADXCVR,
	//	JESD204, AXI_AD9680, TX DMAC) configuration
	//******************************************************************************
	ad9680_param.lane_rate_kbps = 10000000;

	xcvr_getconfig(&ad9680_xcvr);
	ad9680_xcvr.reconfig_bypass = 1;
	ad9680_xcvr.dev.qpll_enable = 1;

	ad9680_xcvr.rx_tx_n = 1;
	ad9680_xcvr.lane_rate_kbps = ad9680_param.lane_rate_kbps;

	ad9680_jesd.scramble_enable = 1;
	ad9680_jesd.octets_per_frame = 1;
	ad9680_jesd.frames_per_multiframe = 32;
	ad9680_jesd.subclass_mode = 1;

	ad9680_core.no_of_channels = 2;
	ad9680_core.resolution = 14;

	//******************************************************************************
	// configure the receiver DMA
	//******************************************************************************

	//ad9680_dma.type       = DMAC_RX;
	//ad9680_dma.transfer   = &rx_xfer;
	//rx_xfer.id            = 0;
	//rx_xfer.no_of_samples = 32768;

	//ad9144_dma.type       = DMAC_TX;
	//ad9144_dma.transfer   = &tx_xfer;
	//ad9144_dma.flags      = DMAC_FLAGS_TLAST;
	//tx_xfer.id            = 0;
	//tx_xfer.no_of_samples = dac_buffer_load(ad9144_core, tx_xfer.start_address);
	dac_buffer_load(ad9144_core, 0x000000);
	fmcdaq2_reconfig(&ad9144_param,
					 &ad9144_xcvr,
					 &ad9680_param,
					 &ad9680_xcvr,
					 ad9523_param.pdata);
	//******************************************************************************
	// bring up the system
	//******************************************************************************

	// setup GPIOs

	gpio_desc *clkd_sync;
	gpio_desc *dac_reset;
	gpio_desc *dac_txen;
	gpio_desc *adc_pd;

	gpio_get(&clkd_sync, GPIO_CLKD_SYNC);
	gpio_get(&dac_reset, GPIO_DAC_RESET);
	gpio_get(&dac_txen,  GPIO_DAC_TXEN);
	gpio_get(&adc_pd,    GPIO_ADC_PD);

	gpio_set_value(clkd_sync, 0);
	gpio_set_value(dac_reset, 0);
	gpio_set_value(dac_txen,  0);
	gpio_set_value(adc_pd,    1);
	mdelay(5);

	gpio_set_value(clkd_sync, 1);
	gpio_set_value(dac_reset, 1);
	gpio_set_value(dac_txen,  1);
	gpio_set_value(adc_pd,    0);

	// setup clocks
	ad9523_setup(&ad9523_device, ad9523_param);

	// set up the devices
	ad9680_setup(&ad9680_device, ad9680_param);
	ad9144_setup(&ad9144_device, ad9144_param);

	// set up the JESD core
	jesd_setup(ad9680_jesd);
	jesd_setup(ad9144_jesd);

	// set up the XCVRs
	if (ad9144_xcvr.dev.qpll_enable) {	// DAC_XCVR controls the QPLL reset
		xcvr_setup(&ad9144_xcvr);
		xcvr_setup(&ad9680_xcvr);
	} else {				      		// ADC_XCVR controls the CPLL reset
		xcvr_setup(&ad9680_xcvr);
		xcvr_setup(&ad9144_xcvr);
	}

	axi_jesd204_tx_status_read(ad9144_jesd);
	axi_jesd204_rx_status_read(ad9680_jesd);

	// interface core set up
	adc_setup(ad9680_core);  //no pointer
	dac_setup(&ad9144_core); //pointer

	ad9144_status(ad9144_device);
/*
	//******************************************************************************
	// transport path testing
	//******************************************************************************
	uint8_t status = 0;
	ad9144_channels[0].sel = DAC_SRC_SED;
	ad9144_channels[1].sel = DAC_SRC_SED;
	dac_data_setup(&ad9144_core);
	ad9144_short_pattern_test(ad9144_device, ad9144_param);

	// PN7 data path test
	// I think there is something wrong with the naming for PRBS sequence in the core
	ad9144_channels[0].sel = DAC_SRC_PN23;
	ad9144_channels[1].sel = DAC_SRC_PN23;
	dac_data_setup(&ad9144_core);
	ad9144_param.prbs_type = AD9144_PRBS7;
	ad9144_datapath_prbs_test(ad9144_device, ad9144_param);

	ad9144_spi_read(ad9144_device, REG_PRBS, &status);
	printf("Value in REG_PRBS (%x)!.\n", status);

	// PN15 data path test
	// I think there is something wrong with the naming for PRBS sequence in the core
	ad9144_channels[0].sel = DAC_SRC_PN31;
	ad9144_channels[1].sel = DAC_SRC_PN31;
	dac_data_setup(&ad9144_core);
	ad9144_param.prbs_type = AD9144_PRBS15;
	ad9144_datapath_prbs_test(ad9144_device, ad9144_param);

	ad9144_spi_read(ad9144_device, REG_PRBS, &status);
	printf("Value in REG_PRBS (%x)!.\n", status); // print


	//******************************************************************************
	// receive path testing
	//******************************************************************************

	ad9680_test(ad9680_device, AD9680_TEST_PN9);
		if(adc_pn_mon(ad9680_core, ADC_PN9) == -1) {
			printf("%s ad9680 - PN9 sequence mismatch!\n", __func__);
		};

	ad9680_test(ad9680_device, AD9680_TEST_PN23);
		if(adc_pn_mon(ad9680_core, ADC_PN23A) == -1) {
			printf("%s ad9680 - PN23 sequence mismatch!\n", __func__);
		};
*/
	//******************************************************************************
	// PRBS loop back path testing
	//******************************************************************************
	//ad9144_spi_write(ad9144_device, REG_PRBS, 0x00);
	/*ad9144_channels[0].sel = DAC_SRC_DMA;
	ad9144_channels[1].sel = DAC_SRC_DMA;
	dac_data_setup(&ad9144_core);

	if(!dmac_start_transaction(ad9144_dma)){
		printf("daq2: transmit data from memory\n");
	};
*/
	ad9144_channels[0].sel = DAC_SRC_DDS;
	ad9144_channels[1].sel = DAC_SRC_DDS;
	dac_data_setup(&ad9144_core);
	printf("daq2: DDS setup done\n");
	//ad9680_test(ad9680_device, AD9680_TEST_OFF);
/*

#if DMA_BUFFER
	ad9144_channels[0].sel = DAC_SRC_DMA;
	ad9144_channels[1].sel = DAC_SRC_DMA;
	dac_data_setup(&ad9144_core);

	if(!dmac_start_transaction(ad9144_dma)){
		printf("daq2: transmit data from memory\n");
	};
#else
	ad9144_channels[0].sel = DAC_SRC_DDS;
	ad9144_channels[1].sel = DAC_SRC_DDS;
	dac_data_setup(&ad9144_core);

	printf("daq2: setup and configuration is done\n");
#endif
*/

//	if(!dmac_start_transaction(ad9680_dma)){
//		printf("daq2: RX capture done.\n");
//	};
	//******************************************************************************
	// DONE
	//******************************************************************************


	ad9523_remove(ad9523_device);

	uint8_t mode = 0;
	do {
		if (mode == 0){
		printf ("Enter 1 to start RX DMA transfer\n"
				"Enter 2 to set TX as DDS 250 MHz \n"
				"Enter 3 to set TX as DDS 100 MHz \n"
				"Enter 4 to set TX to 0\n"
				"Enter 5 to set TX to DMA\n"
				"Enter 6 to start TX DMA transfer\n"
				"Enter 0 to exit\n");
		}
		mode = ad_uart_read();
		if(mode == '1') {
		/*	if(!dmac_start_transaction(ad9680_dma)){
				printf("daq2: RX capture done.\n\n");
			};
		 */
		}else if(mode == '2') {
			ad9144_channels[0].dds_frequency_0 = 250*1000*1000; //250 MHz sine wave
			ad9144_channels[1].dds_frequency_0 = 250*1000*1000; //250 MHz sine wave
			ad9144_channels[0].sel = DAC_SRC_DDS;
			ad9144_channels[1].sel = DAC_SRC_DDS;
			dac_data_setup(&ad9144_core);
			printf("daq2: DDS setup done\n\n");
		}else if(mode == '3'){
			ad9144_channels[0].dds_frequency_0 = 100*1000*1000; //100 MHz sine wave
			ad9144_channels[1].dds_frequency_0 = 100*1000*1000; //100 MHz sine wave
			ad9144_channels[0].sel = DAC_SRC_DDS;
			ad9144_channels[1].sel = DAC_SRC_DDS;
			dac_data_setup(&ad9144_core);
			printf("daq2: DDS setup done\n\n");
		}else if(mode == '4'){
			ad9144_channels[0].sel = DAC_SRC_ZERO;
			ad9144_channels[1].sel = DAC_SRC_ZERO;
			dac_data_setup(&ad9144_core);
			printf("daq2: Zero TX output setup done\n\n");
		}else if(mode == '5'){
			ad9144_channels[0].sel = DAC_SRC_DMA;
			ad9144_channels[1].sel = DAC_SRC_DMA;
			dac_data_setup(&ad9144_core);
			printf("daq2: DMA setup done\n\n");
		}else if(mode == '6'){
		/*	if(!dmac_start_transaction(ad9144_dma)){
				printf("daq2: transmit data from memory\n\n");
			};
		*/
		}else if(mode == 13){
			mode = 0;
		}


	} while (mode != '0');

	gpio_remove(clkd_sync);
	gpio_remove(dac_reset);
	gpio_remove(dac_txen);
	gpio_remove(adc_pd);
	printf("It is Finished\n");

	return(0);
}
