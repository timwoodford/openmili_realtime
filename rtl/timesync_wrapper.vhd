----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2019 03:18:19 PM
-- Design Name: 
-- Module Name: timesync_wrapper - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.common_types.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity timesync_wrapper is
    Port ( clk          : in std_logic;
           reset        : in std_logic;
           clk_enable   : in std_logic;
           sample_in_re : in STD_LOGIC_VECTOR (63 downto 0);
           sample_in_im : in STD_LOGIC_VECTOR (63 downto 0);
           valid_in     : in std_logic;
           pkt_detect : out STD_LOGIC;
           aligned_re : out STD_LOGIC_VECTOR (63 downto 0);
           aligned_im : out STD_LOGIC_VECTOR (63 downto 0);
           aligned_valid : out STD_LOGIC
           );
end timesync_wrapper;

architecture Behavioral of timesync_wrapper is

function adc_to_vector(adc_vec: std_logic_vector(4*16-1 downto 0)) return vector_of_std_logic_vector16 is
    variable ret : vector_of_std_logic_vector16(0 TO 3);
    variable int_val : integer;
  begin
    for ii in 0 to 3 loop
      int_val := to_integer(signed(adc_vec(ii*16+16-1 downto ii*16)));
      ret(ii) := std_logic_vector(to_signed(int_val, 16));
    end loop;
    return ret;
  end function;

component timesync
  PORT( clk                               :   IN    std_logic;
      reset                             :   IN    std_logic;
      clk_enable                        :   IN    std_logic;
      sample_in_re                      :   IN    vector_of_std_logic_vector16(0 TO 3);  -- sfix16_En18 [4]
      sample_in_im                      :   IN    vector_of_std_logic_vector16(0 TO 3);  -- sfix16_En18 [4]
      valid_in                          :   IN    std_logic;
      ce_out                            :   OUT   std_logic;
      valid_out                         :   OUT   std_logic;
      signal_detected                   :   OUT   std_logic;
      aligned_signal_re                 :   OUT   vector_of_std_logic_vector16(0 TO 3);  -- sfix16_En18 [4]
      aligned_signal_im                 :   OUT   vector_of_std_logic_vector16(0 TO 3);  -- sfix16_En18 [4]
      cfo_est                           :   OUT   std_logic_vector(15 DOWNTO 0);  -- ufix16_En27
      cfo_valid                         :   OUT   std_logic
      );
end component;

signal sample_re : vector_of_std_logic_vector16(0 to 3);
signal sample_im : vector_of_std_logic_vector16(0 to 3);
signal aligned_signal_re : vector_of_std_logic_vector16(0 to 3);
signal aligned_signal_im : vector_of_std_logic_vector16(0 to 3);

begin

process (sample_in_re, sample_in_im) begin
    sample_re <= adc_to_vector(sample_in_re);
    sample_im <= adc_to_vector(sample_in_im);
end process;

u_ts_impl : timesync
  port map (
    clk => clk,
    reset => reset,
    clk_enable => clk_enable,
    sample_in_re => sample_re,
    sample_in_im => sample_im,
    valid_in => valid_in,
    ce_out => open,
    valid_out => aligned_valid,
    signal_detected => pkt_detect,
    aligned_signal_re => aligned_signal_re,
    aligned_signal_im => aligned_signal_im,
    cfo_est => open,
    cfo_valid => open
  );

process (aligned_signal_re, aligned_signal_im) begin
    for ii in 0 to 3 loop
      aligned_im(ii*16+15 downto ii*16) <= aligned_signal_im(ii);
      aligned_re(ii*16+15 downto ii*16) <= aligned_signal_re(ii);
    end loop;
end process;

end Behavioral;
